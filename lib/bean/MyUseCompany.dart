class MyUseCompany {
  String uuid;
  String user_uuid;
  String work_use_company;

  MyUseCompany({this.uuid, this.user_uuid, this.work_use_company});

  factory MyUseCompany.fromJson(Map<String, dynamic> json) {
    return MyUseCompany(
        uuid: json['uuid'],
        user_uuid: json['user_uuid'],
        work_use_company: json['work_use_company']);
  }
}
