class Count {
  String company_name = ""; //string  `json:"company_name"` //开单公司名字
  int count = 0; //       int64   `json:"count"`        //这个公司的开单条数
  Object money = 0.0; //       float64 `json:"money"`        //这个公司所有开单的金额

  Count({this.company_name, this.count, this.money});

  @override
  String toString() {
    return 'Count{company_name: $company_name, count: $count, money: $money}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Count &&
          runtimeType == other.runtimeType &&
          company_name == other.company_name &&
          count == other.count &&
          money == other.money;

  @override
  int get hashCode => company_name.hashCode ^ count.hashCode ^ money.hashCode;

  factory Count.fromJson(Map<String, dynamic> json) {
    return Count(
        company_name: json['company_name'],
        count: json['count'],
        money: json['money']);
  }

}
