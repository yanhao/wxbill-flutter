class WorkBill {
  int id;
  String uuid;
  String user_uuid;
  String work_time;
  String work_unit;
  String work_content;
  String work_car_number;
  String work_device;
  Object work_use_time=0.0;
  Object work_time_price=0.0;
  Object work_money=0.0;
  String create_time;
  String company_uuid;
  String company_name;
  bool check;
  bool isVisibilty;

  WorkBill(
      {this.id,
      this.uuid,
      this.user_uuid,
      this.work_time,
      this.work_unit,
      this.work_content,
      this.work_car_number,
      this.work_device,
      this.work_use_time=0.0,
      this.work_time_price=0.0,
      this.work_money=0.0,
      this.create_time,
      this.company_uuid,
      this.company_name,
      this.check,
      this.isVisibilty=false});

  @override
  String toString() {
    return 'WorkBill{id: $id, uuid: $uuid, user_uuid: $user_uuid, work_time: $work_time, work_unit: $work_unit, work_content: $work_content, work_car_number: $work_car_number, work_device: $work_device, work_use_time: $work_use_time, work_time_price: $work_time_price, work_money: $work_money, create_time: $create_time, company_uuid: $company_uuid, company_name: $company_name, check: $check}';
  }

  factory WorkBill.fromJson(Map<String, dynamic> json) {
    return WorkBill(
      id: json['id'],
      uuid: json['uuid'],
      user_uuid: json['user_uuid'],
      work_time: json['work_time'],
      work_unit: json['work_unit'],
      work_content: json['work_content'],
      work_car_number: json['work_car_number'],
      work_device: json['work_device'],
      work_use_time: json['work_use_time'],
      work_time_price: json['work_time_price'],
      work_money: json['work_money'],
      create_time: json['create_time'],
      company_uuid: json['company_uuid'],
      company_name: json['company_name'],
      check: json['check'],
    );
  }
}
