class MyWorkBillCount {
  int billworks = 0;
  Object money = 0.0;

  MyWorkBillCount({this.billworks = 0, this.money = 0});

  @override
  String toString() {
    return 'MyWorkBillCount{billworks: $billworks, money: $money}';
  }

  factory MyWorkBillCount.fromJson(Map<String, dynamic> json) {
    return MyWorkBillCount(billworks: json['billworks'], money: json['money']);
  }
}
