class ResponseMsg<T> {
  int code;
  String message;
  T data;

  ResponseMsg({this.code, this.message, this.data});

  @override
  String toString() {
    return 'ResponseMsg{code: $code,  message: $message}';
  }

  factory ResponseMsg.fromJson(Map<String, dynamic> json) {
    return ResponseMsg(
        code: json['code'], data: json['data'], message: json['message']);
  }
}
