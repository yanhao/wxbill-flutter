class LoginRespone {
  String token;
  String uuid;

  LoginRespone({this.token, this.uuid});

  @override
  String toString() {
    return 'LoginRespone{token: $token, uuid: $uuid}';
  }

  factory LoginRespone.fromJson(Map<String, dynamic> json) {
    return LoginRespone(token: json['token'], uuid: json['uuid']);
  }
}
