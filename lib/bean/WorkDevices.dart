class WorkDevices {
  String name;
  Object devices_price;

  WorkDevices({this.name, this.devices_price});

  @override
  String toString() {
    return 'WorkDevices{name: $name, devices_price: $devices_price}';
  }

  factory WorkDevices.fromJson(Map<String, dynamic> json) {
    return WorkDevices(
        name: json['name'], devices_price: json['devices_price']);
  }
}
