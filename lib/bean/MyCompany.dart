class MyCompany {
  String uuid;
  String company_name;
  String
      company_address; //string    `json:"company_address"` //varchar(255) DEFAULT NULL COMMENT '公司地址',
  String company_phone;

  MyCompany(
      {this.uuid, this.company_name, this.company_address, this.company_phone});

  factory MyCompany.fromJson(Map<String, dynamic> json) {
    return MyCompany(
        uuid: json['uuid'],
        company_name: json['company_name'],
        company_address: json['company_address'],
        company_phone: json['company_phone']);
  }
}
