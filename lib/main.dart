import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:yh/providers/MyCompanyProvider.dart';
import 'package:yh/providers/MyUseCompanyProvider.dart';
import 'package:yh/providers/ProviderConfig.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/providers/WorkBillProvider.dart';
import 'package:yh/ui/login/LoginPage.dart';
import 'package:yh/utils/Strings.dart';

import 'ui/main/IndexPage.dart';
import 'utils/Global.dart';

void main() {
  runApp(MultiProvider(
//    providers: providers,
    providers: [
      ChangeNotifierProvider<UserInfoProvider>(
        create: (_) => UserInfoProvider(),
      ),
      ChangeNotifierProvider<WorkBillProvider>(
        create: (_) => WorkBillProvider(),
      ),
      ChangeNotifierProvider<MyCompanyProvider>(
        create: (_) => MyCompanyProvider(),
      ),
      ChangeNotifierProvider<MyUseCompanyProvider>(
        create: (_) => MyUseCompanyProvider(),
      )
    ],
    child: MyApp(),
  ));
  configLoading();
}

void configLoading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.dark
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = Colors.yellow
    ..backgroundColor = Colors.green
    ..indicatorColor = Colors.yellow
    ..textColor = Colors.yellow
    ..maskColor = Colors.blue.withOpacity(0.5)
    ..userInteractions = true;
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FlutterEasyLoading(
      child: MaterialApp(
        title: APPNAME,
        theme: ThemeData(
          primarySwatch: Orange,
        ),
//      home: MyHomePage(title: APPNAME),
        home: LoginPage(),
      ),
    );
  }
}
