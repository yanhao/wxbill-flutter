import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/ui/main/IndexPage.dart';
import 'package:yh/utils/Global.dart';
import 'package:yh/utils/Setting.dart';
import 'package:yh/utils/Strings.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with WidgetsBindingObserver {
  String phone = "";
  String pwd = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    Future.delayed(Duration(seconds: 1), () {
      checkToken();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
    }
  }

  void setting() async {
    String token = await getSpString("token");
    String uuid = await getSpString("uuid");
    String phone = await getSpString("phone");
    if (token != null &&
        token.length > 0 &&
        uuid != null &&
        uuid.length > 0 &&
        phone != null &&
        phone.length > 0) {
      Provider.of<UserInfoProvider>(context, listen: false).setToken(token);
      Provider.of<UserInfoProvider>(context, listen: false).setUUId(uuid);
      Provider.of<UserInfoProvider>(context, listen: false).setPhone(phone);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => IndexPage()),
          (router) => router == null);
    }
  }

  void checkToken() async {
    String token = await getSpString("token");
    if (token == null || token.length <= 0) {
      return;
    }
    EasyLoading.show(status: '正在检测用户信息...');
    await api_checkToken(token, (successMsg) {
      EasyLoading.showToast(successMsg, duration: Duration(seconds: 4));
      EasyLoading.dismiss();
      setting();
    }, (failMsg) async {
      //清除保存的用户信息
      await clearSpUserInfo();
      EasyLoading.dismiss();
      EasyLoading.showToast(failMsg, duration: Duration(seconds: 4));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                child: Image.asset(
                  'assets/images/icon_app.png',
                  width: 80,
                  height: 80,
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                child: TextField(
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 20, right: 20),
                      border: OutlineInputBorder(
                        gapPadding: 10.0,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      labelText: PHONEHINT,
                      helperText: PHONEHELP),
                  maxLines: 1,
                  maxLength: 11,
                  keyboardType: TextInputType.phone,
                  onChanged: (v) {
                    setState(() {
                      phone = v;
                    });
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20, top: 5),
                child: TextField(
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 20, right: 20),
                      border: OutlineInputBorder(
                        gapPadding: 10.0,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      labelText: PASSWORDHINT,
                      helperText: PASSWORDHELP),
                obscureText: true,//是否是密码
                  onChanged: (v) {
                    setState(() {
                      pwd = v;
                    });
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30),
                height: 40,
                width: MediaQuery.of(context).size.width / 2.5,
                child: FlatButton(
                  onPressed: () async {
                    print(LOGIN);
                    EasyLoading.show(status: '正在登录中...');
                    await api_login(phone, pwd, (result) async{
                      EasyLoading.dismiss();
                      SharedPreferences sp = await SharedPreferences.getInstance();
                      sp.setString("token", result.token);
                      sp.setString("uuid", result.uuid);
                      sp.setString("phone", phone);
                      Provider.of<UserInfoProvider>(context, listen: false)
                          .setToken(result.token);
                      Provider.of<UserInfoProvider>(context, listen: false)
                          .setUUId(result.uuid);
                      Provider.of<UserInfoProvider>(context, listen: false)
                          .setPhone(phone);
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (context) => IndexPage()),
                          (router) => router == null);
                    }, (msg) {
                      EasyLoading.dismiss();
                      EasyLoading.showToast(msg);
                    });
                  },
                  child: Text(
                    LOGIN,
                    style: TextStyle(fontSize: 15),
                  ),
                  textColor: Colors.white,
                  color: Orange,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "注:登录即注册",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 15, color: Orange, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
      ),
    );
  }

  _LoginPageState();
}
