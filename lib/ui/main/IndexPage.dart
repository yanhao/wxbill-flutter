import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yh/providers/MyCompanyProvider.dart';
import 'package:yh/providers/MyUseCompanyProvider.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/ui/count/CountPage.dart';
import 'package:yh/ui/home/HomePage.dart';
import 'package:yh/ui/me/Me.dart';
import 'package:yh/ui/me/help/Help.dart';
import 'package:yh/utils/Setting.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

//with WidgetsBindingObserver
class _IndexPageState extends State<IndexPage> {
  final List<BottomNavigationBarItem> bottomTabs = [
//    BottomNavigationBarItem(icon: Icon(CupertinoIcons.home), title: Text("首页")),
    BottomNavigationBarItem(
        icon: Image.asset(
          'assets/images/icon_home_no.png',
          width: 24,
          height: 24,
        ),
        activeIcon: Image.asset(
          'assets/images/icon_home.png',
          width: 24,
          height: 24,
        ),
        title: Text("首页")),
    BottomNavigationBarItem(
        icon: Image.asset(
          'assets/images/icon_count_no.png',
          width: 24,
          height: 24,
        ),
        activeIcon: Image.asset(
          'assets/images/icon_count.png',
          width: 24,
          height: 24,
        ),
        title: Text("统计")),
    BottomNavigationBarItem(
        icon: Image.asset(
          'assets/images/icon_my_no.png',
          width: 24,
          height: 24,
        ),
        activeIcon: Image.asset(
          'assets/images/icon_my.png',
          width: 24,
          height: 24,
        ),
        title: Text("我的")),
  ];

  final List tabPages = [HomePage(), CountPage(), MePage()];
  int currentIndex = 0;
  PageController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = PageController(initialPage: currentIndex);
    getMyCompany();
    getMyUseCompany();
    chenkHelp();
  }

  void chenkHelp() async {
    int helpCount = await getSpint("help");
    if (helpCount == null) {
      show(0);
    } else if (helpCount != null && helpCount <= 3) {
      show(helpCount);
    }
  }

  void show(int helpCount) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("帮助"),
            content: Text("是否查看使用帮助指导页面?"),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  "取消",
                  style: TextStyle(color: Colors.black),
                ),
                onPressed: () => Navigator.of(context).pop(), //关闭对话框
              ),
              FlatButton(
                child: Text("查看"),
                onPressed: () async {
                  int count = helpCount + 1;
                  SharedPreferences sp = await SharedPreferences.getInstance();
                  sp.setInt("help", count);
                  Navigator.of(context).pop(true);
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Help()));
                },
              ),
            ],
          );
        });
  }

  void getMyCompany() async {
    String user_uuid =
        Provider.of<UserInfoProvider>(context, listen: false).uuid;
    await api_MyCompany(user_uuid, (datas) {
      if (datas.length > 0) {
        Provider.of<MyCompanyProvider>(context, listen: false).addAll(0, datas);
      }
    }, (msg) {});
  }

  void getMyUseCompany() async {
    String user_uuid =
        Provider.of<UserInfoProvider>(context, listen: false).uuid;
    await api_MyUseCompany(user_uuid, (datas) {
      if (datas.length > 0) {
        Provider.of<MyUseCompanyProvider>(context, listen: false)
            .addAll(0, datas);
      }
    }, (msg) {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller.dispose();
  }

  void _pageChange(int index) {
    if (index != currentIndex) {
      setState(() {
        currentIndex = index;
      });
    }
  }

  void _onTap(int index) {
    setState(() {
      currentIndex = index;
    });
    _controller.jumpToPage(index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView.builder(
        itemBuilder: (BuildContext context, int index) {
          return tabPages[index];
        },
        itemCount: tabPages.length,
        controller: _controller,
        physics: NeverScrollableScrollPhysics(),
        //禁止左右滑动
        onPageChanged: _pageChange,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: bottomTabs,
        type: BottomNavigationBarType.fixed,
        currentIndex: currentIndex,
        onTap: _onTap,
      ),
    );
  }
}
