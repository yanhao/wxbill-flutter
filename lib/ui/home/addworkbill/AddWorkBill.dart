import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:yh/bean/MyCompany.dart';
import 'package:yh/bean/MyUseCompany.dart';
import 'package:yh/bean/WorkBill.dart';
import 'package:yh/bean/WorkDevices.dart';
import 'package:yh/bus/UpdateAddBillwork.dart';
import 'package:yh/component/HorizontalLine.dart';
import 'package:yh/component/ItemText.dart';
import 'package:yh/providers/MyCompanyProvider.dart';
import 'package:yh/providers/MyUseCompanyProvider.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/utils/Global.dart';
import 'package:yh/utils/GlobalEventBus.dart';

class AddWorkBil extends StatefulWidget {
  @override
  _AddWorkBilState createState() => _AddWorkBilState();
}

class _AddWorkBilState extends State<AddWorkBil> {
  String devicesHint = "请选择用工设备";
  String timeHint = "请选择用工时间";
  String work_company = "";
  String default_work_company = "";
  String work_use_comapny = "";
  String default_work_use_comapny = "";
  String work_devices = "";
  String work_time = "";
  String work_content = "";
  String work_car_number = "";
  String work_use_time = "";
  int year, month, day;
  List<WorkDevices> devices = [];

  List<MyCompany> myCompanys = [];
  List<MyUseCompany> myUseCompanys = [];

  WorkDevices selectWorkDevices = null;

  TextEditingController _comPanyController;
  TextEditingController _useComPanyController;
  TextEditingController _contentController;
  TextEditingController _carnumberController;
  TextEditingController _usetimeController;

  void submit() async {
    if (work_company == null || work_company.length <= 0) {
      EasyLoading.showToast("请输入/选择公司名称", duration: Duration(seconds: 2));
      return;
    }
    if (work_use_comapny == null || work_use_comapny.length <= 0) {
      EasyLoading.showToast("请输入/选择租借单位", duration: Duration(seconds: 2));
      return;
    }
    if (work_devices == devicesHint) {
      EasyLoading.showToast("请选择用工设备", duration: Duration(seconds: 2));
      return;
    }
    if (work_time == timeHint) {
      EasyLoading.showToast("请选择用工时间", duration: Duration(seconds: 2));
      return;
    }
    if (work_car_number == null || work_car_number.length <= 0) {
      EasyLoading.showToast("请输入设备车牌", duration: Duration(seconds: 2));
      return;
    }
    if (work_use_time.length < 0 ||
        work_use_time == "" ||
        work_use_time == null) {
      EasyLoading.showToast("请输入使用时长", duration: Duration(seconds: 2));
      return;
    }

    WorkBill workBill = WorkBill(
        user_uuid: Provider.of<UserInfoProvider>(context, listen: false).uuid,
        work_time: work_time,
        work_unit: work_use_comapny,
        work_content: work_content,
        work_car_number: work_car_number,
        work_device: selectWorkDevices.name,
        work_use_time: double.parse(work_use_time),
        company_name: work_company,
        work_time_price: selectWorkDevices.devices_price);

    await api_AddWorkBill(workBill, (data) {
      // 查看公司名称和 租借单位在provider中是否存在，如果不存在就添加，存在就不添加
      bool isCompany =
          myCompanys.any((item) => (item.company_name == work_company));
      if (!isCompany) {
        Provider.of<MyCompanyProvider>(context, listen: false).add(
            myCompanys.length,
            MyCompany(
                company_name: work_company,
                company_address: "",
                company_phone: ""));
      }
      bool isUseCompany = myUseCompanys
          .any((item) => (item.work_use_company == work_use_comapny));
      if (!isUseCompany) {
        Provider.of<MyUseCompanyProvider>(context, listen: false).add(
            myUseCompanys.length,
            MyUseCompany(
                user_uuid:
                    Provider.of<UserInfoProvider>(context, listen: false).uuid,
                work_use_company: work_use_comapny));
      }
      EasyLoading.showToast("增加工单成功");
      setState(() {
        work_devices = "";
        work_time = "";
        selectWorkDevices = null;
      });
      _comPanyController.clear();
      _useComPanyController.clear();
      _contentController.clear();
      _carnumberController.clear();
      _usetimeController.clear();
      GlobalEventBus().event.fire(UpdateAddBillwork());
    }, (errMsg) {
      print(errMsg);
      EasyLoading.showToast(errMsg);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _comPanyController = TextEditingController();
    _useComPanyController = TextEditingController();
    _contentController = TextEditingController();
    _carnumberController = TextEditingController();
    _usetimeController = TextEditingController();
    var now = new DateTime.now();
    setState(() {
      year = now.year;
      month = now.month;
      day = now.day;
    });
    getDeviceInfos();
    getCompanys();

    _comPanyController.addListener(() {
      setState(() {
        work_company = _comPanyController.text;
      });
    });
    _useComPanyController.addListener(() {
      setState(() {
        work_use_comapny = _useComPanyController.text;
      });
    });
    _contentController.addListener(() {
      setState(() {
        work_content = _contentController.text;
      });
    });
    _carnumberController.addListener(() {
      setState(() {
        work_car_number = _carnumberController.text;
      });
    });
    _usetimeController.addListener(() {
      String useTime = _usetimeController.text;
      setState(() {
        work_use_time = useTime;
      });
    });
  }

  getCompanys() {
    setState(() {
      myCompanys = Provider.of<MyCompanyProvider>(context, listen: false).datas;
      myUseCompanys =
          Provider.of<MyUseCompanyProvider>(context, listen: false).datas;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (_comPanyController != null) {
      _comPanyController = null;
    }
    if (_useComPanyController != null) {
      _useComPanyController = null;
    }
    if (_contentController != null) {
      _contentController = null;
    }
    if (_carnumberController != null) {
      _carnumberController = null;
    }
    if (_usetimeController != null) {
      _usetimeController = null;
    }
  }

  void getDeviceInfos() async {
    await api_Devices((List<WorkDevices> datas) {
//      for(var item  in datas){
//        print("获取到:"+item.name+"  ${item.devices_price}");
//      }
      setState(() {
        devices = datas;
      });
    }, (errMsg) {
      EasyLoading.showToast(errMsg);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("添加工作单"),
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                height: 50,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.red,
                            size: 14,
                          ),
                          Text(
                            "公司名称",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: TextField(
                            keyboardType: TextInputType.text,
                            controller: _comPanyController,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "请输入/选择公司名称"),
                          )),
                          InkWell(
                            onTap: () {
                              showModalBottomSheet(
//                      isScrollControlled: true,
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Container(
                                      constraints: BoxConstraints(
                                        maxHeight:
                                            MediaQuery.of(context).size.height /
                                                2,
                                      ),
                                      child: ListView.builder(
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            children: <Widget>[
                                              InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                  setState(() {
                                                    _comPanyController.text =
                                                        '${myCompanys[index].company_name}';
                                                    default_work_company =
                                                        '${myCompanys[index].company_name}';
                                                    work_company =
                                                        myCompanys[index]
                                                            .company_name;
                                                  });
                                                },
                                                child: Container(
                                                  height: 40,
                                                  margin:
                                                      EdgeInsets.only(top: 5),
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                    "${myCompanys[index].company_name}",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              ),
                                              index == myCompanys.length - 1
                                                  ? Container(
                                                      child: Column(
                                                        children: <Widget>[
                                                          Container(
                                                            height: 10,
                                                            color: Color(int.parse(
                                                                "0xffF6F9F9")),
                                                          ),
                                                          InkWell(
                                                            onTap: () {
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                            child: Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              height: 50,
                                                              child: Text(
                                                                "取消",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        16,
                                                                    color: Colors
                                                                        .black,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  : Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 1,
                                                      color: Color(int.parse(
                                                          "0xfff2f3f5")),
                                                    ),
                                            ],
                                          );
                                        },
                                        itemCount: myCompanys.length,
                                      ),
                                    );
                                  });
                            },
                            child: (myCompanys != null && myCompanys.length > 0)
                                ? Icon(Icons.arrow_forward_ios,
                                    color: Colors.black45)
                                : Container(),
                          )
                        ],
                      ),
                    ),
                    HorizontalLine(
                      height: 0.8,
                      color: Color(int.parse("0xfff2f3f5")),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                height: 50,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.red,
                            size: 14,
                          ),
                          Text(
                            "租借单位",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: TextField(
                            keyboardType: TextInputType.text,
                            controller: _useComPanyController,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "请输入/选择租借单位"),
                          )),
                          InkWell(
                            onTap: () {
                              showModalBottomSheet(
//                      isScrollControlled: true,
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Container(
                                      constraints: BoxConstraints(
                                        maxHeight:
                                            MediaQuery.of(context).size.height /
                                                2,
                                      ),
                                      child: ListView.builder(
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            children: <Widget>[
                                              InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                  setState(() {
                                                    _useComPanyController.text =
                                                        '${myUseCompanys[index].work_use_company}';
                                                    default_work_use_comapny =
                                                        "${myUseCompanys[index].work_use_company}";
                                                    work_use_comapny =
                                                        "${myUseCompanys[index].work_use_company}";
                                                  });
                                                },
                                                child: Container(
                                                  height: 40,
                                                  margin:
                                                      EdgeInsets.only(top: 5),
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                    "${myUseCompanys[index].work_use_company}",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              ),
                                              index == myUseCompanys.length - 1
                                                  ? Container(
                                                      child: Column(
                                                        children: <Widget>[
                                                          Container(
                                                            height: 10,
                                                            color: Color(int.parse(
                                                                "0xffF6F9F9")),
                                                          ),
                                                          InkWell(
                                                            onTap: () {
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                            child: Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              height: 50,
                                                              child: Text(
                                                                "取消",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        16,
                                                                    color: Colors
                                                                        .black,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  : Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 1,
                                                      color: Color(int.parse(
                                                          "0xfff2f3f5")),
                                                    ),
                                            ],
                                          );
                                        },
                                        itemCount: myUseCompanys.length,
                                      ),
                                    );
                                  });
                            },
                            child: (myUseCompanys != null &&
                                    myUseCompanys.length > 0)
                                ? Icon(Icons.arrow_forward_ios,
                                    color: Colors.black45)
                                : Container(),
                          )
                        ],
                      ),
                    ),
                    HorizontalLine(
                      height: 0.8,
                      color: Color(int.parse("0xfff2f3f5")),
                    ),
                  ],
                ),
              ),
              ItemText(
                isShow: true,
                title: "用工设备",
                content:
                    "${selectWorkDevices == null ? devicesHint : selectWorkDevices.name}",
                onTap: () {
                  //https://blog.csdn.net/cpcpcp123/article/details/97660036
                  showModalBottomSheet(
                      isScrollControlled: true,
                      context: context,
                      builder: (BuildContext context) {
                        return Container(
                          constraints: BoxConstraints(
                            maxHeight: MediaQuery.of(context).size.height / 3,
                          ),
                          child: ListView.builder(
                            itemBuilder: (BuildContext context, int index) {
                              return Column(
                                children: <Widget>[
                                  InkWell(
                                    onTap: () {
                                      Navigator.pop(context);
                                      setState(() {
                                        selectWorkDevices = devices[index];
                                      });
                                    },
                                    child: Container(
                                      height: 40,
                                      margin: EdgeInsets.only(top: 5),
                                      alignment: Alignment.center,
                                      child: Text(
                                        "${devices[index].name}",
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  index == devices.length - 1
                                      ? Container(
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                height: 10,
                                                color: Color(
                                                    int.parse("0xffF6F9F9")),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Container(
                                                  alignment: Alignment.center,
                                                  height: 50,
                                                  child: Text(
                                                    "取消",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      : Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 1,
                                          color: Color(int.parse("0xfff2f3f5")),
                                        ),
                                ],
                              );
                            },
                            itemCount: devices.length,
                          ),
                        );
                      });
                },
              ),
              ItemText(
                isShow: true,
                title: "用工时间",
                content: "${work_time == "" ? timeHint : work_time}",
                onTap: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2017, 1, 1),
                      maxTime: DateTime(year, month, day),
                      theme: DatePickerTheme(
                        headerColor: Color(int.parse("0xffF6F9F9")),
                        backgroundColor: Colors.white,
                        itemStyle: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 18),
                        doneStyle: TextStyle(color: Orange, fontSize: 16),
                      ), onChanged: (date) {
//                    print('change $date in time zone ' +
//                        date.timeZoneOffset.inHours.toString());
                  }, onConfirm: (date) {
                    setState(() {
                      work_time = '${date.toString().split(" ")[0]}';
                    });
                  }, currentTime: DateTime.now(), locale: LocaleType.zh);
                },
              ),
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                height: 50,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.transparent,
                            size: 14,
                          ),
                          Text(
                            "工作内容",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: TextField(
                            keyboardType: TextInputType.text,
                            controller: _contentController,
                            decoration: InputDecoration(
                                border: InputBorder.none, hintText: "请输入工作内容"),
                          )),
                        ],
                      ),
                    ),
                    HorizontalLine(
                      height: 0.8,
                      color: Color(int.parse("0xfff2f3f5")),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                height: 50,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.red,
                            size: 14,
                          ),
                          Text(
                            "设备车牌",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: TextField(
                            keyboardType: TextInputType.text,
                            controller: _carnumberController,
                            decoration: InputDecoration(
                                border: InputBorder.none, hintText: "请输入设备车牌"),
                          )),
                        ],
                      ),
                    ),
                    HorizontalLine(
                      height: 0.8,
                      color: Color(int.parse("0xfff2f3f5")),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                height: 50,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.red,
                            size: 14,
                          ),
                          Text(
                            "使用时长",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: TextField(
                            keyboardType: TextInputType.number,
                            controller: _usetimeController,
                            decoration: InputDecoration(
                                border: InputBorder.none, hintText: "请输入使用时长"),
                          )),
                        ],
                      ),
                    ),
                    HorizontalLine(
                      height: 0.8,
                      color: Color(int.parse("0xfff2f3f5")),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                height: 40,
                width: MediaQuery.of(context).size.width / 3,
                child: FlatButton(
                  onPressed: () {
                    submit();
                  },
                  child: Text(
                    "添加工作单",
                    style: TextStyle(fontSize: 15),
                  ),
                  textColor: Colors.white,
                  color: Orange,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
