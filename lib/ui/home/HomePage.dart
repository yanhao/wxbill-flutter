import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:provider/provider.dart';
import 'package:yh/bean/WorkBill.dart';
import 'package:yh/bus/UpdateAddBillwork.dart';
import 'package:yh/component/HorizontalLine.dart';
import 'package:yh/component/VerticalLine.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/providers/WorkBillProvider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/utils/Global.dart';
import 'package:yh/utils/GlobalEventBus.dart';

import 'addworkbill/AddWorkBill.dart';
import 'changworkbill/ChangeworkBill.dart';
import 'homeserch/HomeSearch.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with WidgetsBindingObserver, AutomaticKeepAliveClientMixin {
  EasyRefreshController _refreshController = EasyRefreshController();
  bool isRefresh, isLoadMore;
  int pageindex = 1, pagenumber = 20;
  String user_uuid;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getUuid();
    getdata(true, false, 1, pagenumber);

    GlobalEventBus().event.on<UpdateAddBillwork>().listen((event) {
      getdata(true, false, 1, pagenumber);
    });
  }

  @override
  void deactivate() {
    // TODO: implement deactivate
    super.deactivate();
  }

  void getUuid() {
    String uuid = Provider.of<UserInfoProvider>(context, listen: false).uuid;
    setState(() {
      user_uuid = uuid;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
    }
  }

  void getdata(
      bool isRefresh, bool isLoadMore, int pageindex, int pagenumber) async {
    await api_HomeBillworks(
        isRefresh, isLoadMore, pageindex, pagenumber, user_uuid,
        (List<WorkBill> workBills) {
      if (workBills.length > 0) {
        if (isRefresh) {
          if (Provider.of<WorkBillProvider>(context, listen: false).datas !=
              null) {
            Provider.of<WorkBillProvider>(context, listen: false).datas.clear();
          }
          Provider.of<WorkBillProvider>(context, listen: false)
              .addAll(0, workBills);
          _refreshController.finishRefresh(success: true);
        } else {
          int index = Provider.of<WorkBillProvider>(context, listen: false)
              .datas
              .length;
          Provider.of<WorkBillProvider>(context, listen: false)
              .addAll(index, workBills);
          _refreshController.finishLoad(success: true);
        }
        int temp = pageindex + 1;
        setState(() {
          this.pageindex = temp;
        });
      }
    }, (errMsg) {
      if (isRefresh) {
        _refreshController.finishRefresh(success: true);
      } else if (isLoadMore) {
        _refreshController.finishLoad(success: true);
      }
      EasyLoading.showToast(errMsg);
    });
  }

  Widget _itemTop(String titleName, String value, Color colors, bool showLine) {
    return Container(
      height: 50,
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: <Widget>[
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "${titleName}",
                style: TextStyle(fontSize: 16, color: Colors.black),
              ),
              Text("${value}", style: TextStyle(fontSize: 15, color: colors)),
            ],
          )),
          showLine
              ? HorizontalLine(
                  height: 0.8,
                  color: Color(
                    int.parse("0xfff2f3f5"),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  Widget _itemBottom(String box1, String box2, String box3) {
    return Container(
      height: 40,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                "${box1}",
                style: TextStyle(fontSize: 15, color: Colors.black),
              ),
            ),
          ),
          VerticalLine(
            height: 35,
            width: 1,
            color: Color(
              int.parse("0xfff2f3f5"),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text("${box2}",
                  style: TextStyle(fontSize: 15, color: Colors.black)),
            ),
          ),
          VerticalLine(
            height: 35,
            width: 1,
            color: Color(
              int.parse("0xfff2f3f5"),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text("${box3}",
                  style: TextStyle(fontSize: 15, color: Colors.black)),
            ),
          ),
        ],
      ),
    );
  }

  Widget _itemView(WorkBill workBill) {
//  Widget _itemView() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => ChangeWorkBillPage(
                data: workBill,
              ),
            ));
      },
      child: Container(
        child: Column(
          children: <Widget>[
            _itemTop("公司名字", "${workBill.company_name}", Colors.black, true),
            _itemTop("租借单位", "${workBill.work_unit}", Colors.black38, true),
            _itemTop("用工时间", "${workBill.work_time}", Colors.black38, true),
            _itemTop("用工设备", "${workBill.work_device}", Colors.black38, true),
            _itemTop("工作内容", "${workBill.work_content}", Colors.black38, false),
            HorizontalLine(
              height: 0.8,
              color: Color(
                int.parse("0xfff2f3f5"),
              ),
            ),
            _itemBottom("设备车牌", "使用时长", "金额(元)"),
            HorizontalLine(
              height: 0.8,
              color: Color(
                int.parse("0xfff2f3f5"),
              ),
            ),
            _itemBottom("${workBill.work_car_number}",
                "${workBill.work_use_time}", "${workBill.work_money}"),
            HorizontalLine(
              height: 10,
              color: Color(
                int.parse("0xfff2f3f5"),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _search() {
    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext context) => HomeSearch()));
      },
      child: Container(
        margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
        //设置 child 居中
        alignment: Alignment(0, 0),
        height: 40,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: Color(int.parse("0xffF6F9F9")),
          //设置四周边框
          border: new Border.all(width: 1, color: Colors.transparent),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.search,
              size: 24,
              color: Orange,
            ),
            Text(
              "搜索/修改工单",
              style: TextStyle(fontSize: 15, color: Orange),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final workBill_Provider = Provider.of<WorkBillProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("首页"),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            _search(),
            Expanded(
              child: Stack(
                children: <Widget>[
                  EasyRefresh(
                    controller: _refreshController,
                    enableControlFinishRefresh: true,
                    enableControlFinishLoad: true,
                    header: ClassicalHeader(),
                    footer: ClassicalFooter(),
                    child: ListView.builder(
                      itemBuilder: (BuildContext context, int index) {
                        return _itemView(workBill_Provider.datas[index]);
                      },
                      itemCount: workBill_Provider.datas.length > 0
                          ? workBill_Provider.datas.length
                          : 0,
//                    itemCount: listData.length,
//          physics: new AlwaysScrollableScrollPhysics(), //保持滚动
                    ),
                    onRefresh: () async {
                      setState(() {
                        pageindex = 1;
                      });
                      getdata(true, false, 1, pagenumber);
                    },
                    onLoad: () async {
                      getdata(false, true, pageindex, pagenumber);
                    },
                  ),
                  Positioned(
                    bottom: 20,
                    right: 20,
                    child: FloatingActionButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      AddWorkBil()));
                        },
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                          size: 30,
                        )),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
