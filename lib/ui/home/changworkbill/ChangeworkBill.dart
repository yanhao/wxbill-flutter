import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:yh/bean/MyCompany.dart';
import 'package:yh/bean/MyUseCompany.dart';
import 'package:yh/bean/WorkBill.dart';
import 'package:yh/bean/WorkDevices.dart';
import 'package:yh/component/HorizontalLine.dart';
import 'package:yh/component/ItemText.dart';
import 'package:yh/providers/MyCompanyProvider.dart';
import 'package:yh/providers/MyUseCompanyProvider.dart';
import 'package:yh/providers/UpdataSearch.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/providers/WorkBillProvider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/utils/EventBusManager.dart';
import 'package:yh/utils/Global.dart';
import 'package:yh/utils/GlobalEventBus.dart';

class ChangeWorkBillPage extends StatefulWidget {
  WorkBill data;

  ChangeWorkBillPage({Key key, this.data}) : super(key: key);

  @override
  _ChangeWorkBillPageState createState() => _ChangeWorkBillPageState();
}

class _ChangeWorkBillPageState extends State<ChangeWorkBillPage> {
  var bus = EventBusManager();
  String devicesHint = "请选择用工设备";
  String timeHint = "请选择用工时间";
  String work_company = "";
  String work_use_comapny = "";
  String work_devices = "";
  String work_time = "";
  String work_content = "";
  String work_car_number = "";
  String work_use_time = "";
  int year, month, day;
  List<WorkDevices> devices = [];
  WorkDevices selectWorkDevices = null;

  List<MyCompany> myCompanys = [];
  List<MyUseCompany> myUseCompanys = [];

  TextEditingController _comPanyController;
  TextEditingController _useComPanyController;
  TextEditingController _contentController;
  TextEditingController _carnumberController;
  TextEditingController _usetimeController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDeviceInfos();
    _comPanyController = TextEditingController();
    _useComPanyController = TextEditingController();
    _contentController = TextEditingController();
    _carnumberController = TextEditingController();
    _usetimeController = TextEditingController();
    var now = new DateTime.now();
    setState(() {
      work_company = widget.data.company_name;
      work_use_comapny = widget.data.work_unit;
      work_devices = widget.data.work_device;
      work_time = widget.data.work_time;
      work_content = widget.data.work_content;
      work_car_number = widget.data.work_car_number;
      work_use_time = "${widget.data.work_use_time}";
      selectWorkDevices = WorkDevices(
          name: widget.data.work_device,
          devices_price: widget.data.work_time_price);
      year = now.year;
      month = now.month;
      day = now.day;
    });
    _comPanyController.text = widget.data.company_name;
    _useComPanyController.text = widget.data.work_unit;
    _contentController.text = widget.data.work_content;
    _carnumberController.text = widget.data.work_car_number;
    _usetimeController.text = "${widget.data.work_use_time}";

    getCompanys();
    _comPanyController.addListener(() {
      setState(() {
        work_company = _comPanyController.text;
      });
    });
    _useComPanyController.addListener(() {
      setState(() {
        work_use_comapny = _useComPanyController.text;
      });
    });
    _contentController.addListener(() {
      setState(() {
        work_content = _contentController.text;
      });
    });
    _carnumberController.addListener(() {
      setState(() {
        work_car_number = _carnumberController.text;
      });
    });
    _usetimeController.addListener(() {
      String useTime = _usetimeController.text;
      setState(() {
        work_use_time = useTime;
      });
    });
  }

  getCompanys() {
    setState(() {
      myCompanys = Provider.of<MyCompanyProvider>(context, listen: false).datas;
      myUseCompanys =
          Provider.of<MyUseCompanyProvider>(context, listen: false).datas;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("查看/修改工作单"),
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
//              _itemInput("公司名称", "请输入/选择公司名称", true, false,TextInputType.text),
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                height: 50,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.red,
                            size: 14,
                          ),
                          Text(
                            "公司名称",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: TextField(
                            keyboardType: TextInputType.text,
                            controller: _comPanyController,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "请输入/选择公司名称"),
                          )),
                          InkWell(
                            onTap: () {
                              showModalBottomSheet(
//                      isScrollControlled: true,
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Container(
                                      constraints: BoxConstraints(
                                        maxHeight:
                                            MediaQuery.of(context).size.height /
                                                2,
                                      ),
                                      child: ListView.builder(
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            children: <Widget>[
                                              InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                  setState(() {
                                                    _comPanyController.text =
                                                        '${myCompanys[index].company_name}';
                                                    work_company =
                                                        myCompanys[index]
                                                            .company_name;
                                                  });
                                                },
                                                child: Container(
                                                  height: 40,
                                                  margin:
                                                      EdgeInsets.only(top: 5),
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                    "${myCompanys[index].company_name}",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              ),
                                              index == myCompanys.length - 1
                                                  ? Container(
                                                      child: Column(
                                                        children: <Widget>[
                                                          Container(
                                                            height: 10,
                                                            color: Color(int.parse(
                                                                "0xffF6F9F9")),
                                                          ),
                                                          InkWell(
                                                            onTap: () {
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                            child: Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              height: 50,
                                                              child: Text(
                                                                "取消",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        16,
                                                                    color: Colors
                                                                        .black,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  : Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 1,
                                                      color: Color(int.parse(
                                                          "0xfff2f3f5")),
                                                    ),
                                            ],
                                          );
                                        },
                                        itemCount: myCompanys.length,
                                      ),
                                    );
                                  });
                            },
                            child: (myCompanys != null && myCompanys.length > 0)
                                ? Icon(Icons.arrow_forward_ios,
                                    color: Colors.black45)
                                : Container(),
                          )
                        ],
                      ),
                    ),
                    HorizontalLine(
                      height: 0.8,
                      color: Color(int.parse("0xfff2f3f5")),
                    ),
                  ],
                ),
              ),

              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                height: 50,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.red,
                            size: 14,
                          ),
                          Text(
                            "租借单位",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: TextField(
                            keyboardType: TextInputType.text,
                            controller: _useComPanyController,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "请输入/选择租借单位"),
                          )),
                          InkWell(
                            onTap: () {
                              showModalBottomSheet(
//                      isScrollControlled: true,
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Container(
                                      constraints: BoxConstraints(
                                        maxHeight:
                                            MediaQuery.of(context).size.height /
                                                2,
                                      ),
                                      child: ListView.builder(
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            children: <Widget>[
                                              InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                  setState(() {
                                                    _useComPanyController.text =
                                                        '${myUseCompanys[index].work_use_company}';
                                                    work_use_comapny =
                                                        "${myUseCompanys[index].work_use_company}";
                                                  });
                                                },
                                                child: Container(
                                                  height: 40,
                                                  margin:
                                                      EdgeInsets.only(top: 5),
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                    "${myUseCompanys[index].work_use_company}",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              ),
                                              index == myUseCompanys.length - 1
                                                  ? Container(
                                                      child: Column(
                                                        children: <Widget>[
                                                          Container(
                                                            height: 10,
                                                            color: Color(int.parse(
                                                                "0xffF6F9F9")),
                                                          ),
                                                          InkWell(
                                                            onTap: () {
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                            child: Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              height: 50,
                                                              child: Text(
                                                                "取消",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        16,
                                                                    color: Colors
                                                                        .black,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  : Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 1,
                                                      color: Color(int.parse(
                                                          "0xfff2f3f5")),
                                                    ),
                                            ],
                                          );
                                        },
                                        itemCount: myUseCompanys.length,
                                      ),
                                    );
                                  });
                            },
                            child: (myCompanys != null && myCompanys.length > 0)
                                ? Icon(Icons.arrow_forward_ios,
                                    color: Colors.black45)
                                : Container(),
                          )
                        ],
                      ),
                    ),
                    HorizontalLine(
                      height: 0.8,
                      color: Color(int.parse("0xfff2f3f5")),
                    ),
                  ],
                ),
              ),
              ItemText(
                isShow: true,
                title: "用工设备",
                content:
                    "${selectWorkDevices == null ? devicesHint : selectWorkDevices.name}",
                onTap: () {
                  //https://blog.csdn.net/cpcpcp123/article/details/97660036
                  showModalBottomSheet(
                      isScrollControlled: true,
                      context: context,
                      builder: (BuildContext context) {
                        return Container(
                          constraints: BoxConstraints(
                            maxHeight: MediaQuery.of(context).size.height / 3,
                          ),
                          child: ListView.builder(
                            itemBuilder: (BuildContext context, int index) {
                              return Column(
                                children: <Widget>[
                                  InkWell(
                                    onTap: () {
                                      Navigator.pop(context);
                                      setState(() {
                                        selectWorkDevices = devices[index];
                                      });
                                    },
                                    child: Container(
                                      height: 40,
                                      margin: EdgeInsets.only(top: 5),
                                      alignment: Alignment.center,
                                      child: Text(
                                        "${devices[index].name}",
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  index == devices.length - 1
                                      ? Container(
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                height: 10,
                                                color: Color(
                                                    int.parse("0xffF6F9F9")),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Container(
                                                  alignment: Alignment.center,
                                                  height: 50,
                                                  child: Text(
                                                    "取消",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      : Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 1,
                                          color: Color(int.parse("0xfff2f3f5")),
                                        ),
                                ],
                              );
                            },
                            itemCount: devices.length,
                          ),
                        );
                      });
                },
              ),
              ItemText(
                isShow: true,
                title: "用工时间",
                content: "${work_time == "" ? timeHint : work_time}",
                onTap: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2017, 1, 1),
                      maxTime: DateTime(year, month, day),
                      theme: DatePickerTheme(
                        headerColor: Color(int.parse("0xffF6F9F9")),
                        backgroundColor: Colors.white,
                        itemStyle: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 18),
                        doneStyle: TextStyle(color: Orange, fontSize: 16),
                      ), onChanged: (date) {
//                    print('change $date in time zone ' +
//                        date.timeZoneOffset.inHours.toString());
                  }, onConfirm: (date) {
                    setState(() {
                      work_time = '${date.toString().split(" ")[0]}';
                    });
                  }, currentTime: DateTime.now(), locale: LocaleType.zh);
                },
              ),
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                height: 50,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.transparent,
                            size: 14,
                          ),
                          Text(
                            "工作内容",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: TextField(
                            keyboardType: TextInputType.text,
                            controller: _contentController,
                            decoration: InputDecoration(
                                border: InputBorder.none, hintText: "请输入工作内容"),
                          )),
                        ],
                      ),
                    ),
                    HorizontalLine(
                      height: 0.8,
                      color: Color(int.parse("0xfff2f3f5")),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                height: 50,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.red,
                            size: 14,
                          ),
                          Text(
                            "设备车牌",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: TextField(
                            keyboardType: TextInputType.text,
                            controller: _carnumberController,
                            decoration: InputDecoration(
                                border: InputBorder.none, hintText: "请输入设备车牌"),
                          )),
                        ],
                      ),
                    ),
                    HorizontalLine(
                      height: 0.8,
                      color: Color(int.parse("0xfff2f3f5")),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                height: 50,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.red,
                            size: 14,
                          ),
                          Text(
                            "使用时长",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: TextField(
                            keyboardType: TextInputType.number,
                            controller: _usetimeController,
                            decoration: InputDecoration(
                                border: InputBorder.none, hintText: "请输入使用时长"),
                          )),
                        ],
                      ),
                    ),
                    HorizontalLine(
                      height: 0.8,
                      color: Color(int.parse("0xfff2f3f5")),
                    ),
                  ],
                ),
              ),

              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.all(20),
                      height: 40,
                      child: FlatButton(
                        onPressed: () {
                          del(widget.data.uuid);
                        },
                        child: Text(
                          "删除工作单",
                          style: TextStyle(fontSize: 15),
                        ),
                        textColor: Colors.white,
                        color: Orange,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                      ),
                    ),
                  ),
                  Expanded(
                      child: Container(
                    margin: EdgeInsets.all(20),
                    height: 40,
                    child: FlatButton(
                      onPressed: () {
                        update();
                      },
                      child: Text(
                        "更新工作单",
                        style: TextStyle(fontSize: 15),
                      ),
                      textColor: Colors.white,
                      color: Orange,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                    ),
                  )),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void getDeviceInfos() async {
    await api_Devices((List<WorkDevices> datas) {
      setState(() {
        devices = datas;
      });
    }, (errMsg) {
      EasyLoading.showToast(errMsg);
    });
  }

  void del(String uuid) async {
    EasyLoading.show(status: "正在删除数据中...");
    String user_uuid =
        Provider.of<UserInfoProvider>(context, listen: false).uuid;
    await api_delWorkBill(uuid, user_uuid, () {
      Provider.of<WorkBillProvider>(context, listen: false).delUuid(uuid);
      EasyLoading.dismiss();
      EasyLoading.showToast("删除成功", duration: Duration(milliseconds: 1));
      Future.delayed(Duration(seconds: 1), () {
        Navigator.pop(context);
      });
    }, (errMsg) {
      EasyLoading.dismiss();
      EasyLoading.showToast(errMsg);
    });
  }

  void update() async {
    EasyLoading.show(status: "正在更新数据中...");
    double devices_price = double.parse("${selectWorkDevices.devices_price}");
    double use_Time = double.parse("${work_use_time}");
    double money = devices_price / 8 * use_Time;
    WorkBill workBill = WorkBill(
        uuid: widget.data.uuid,
        user_uuid: Provider.of<UserInfoProvider>(context, listen: false).uuid,
        work_time: work_time,
        work_unit: work_use_comapny,
        work_content: work_content,
        work_car_number: work_car_number,
        work_device: selectWorkDevices.name,
        work_use_time: work_use_time,
        company_name: work_company,
        work_money: "${money}",
        work_time_price: selectWorkDevices.devices_price);
    await api_UpdateWorkBill(workBill, (msg) {
      // 查看公司名称和 租借单位在provider中是否存在，如果不存在就添加，存在就不添加
      bool isCompany =
          myCompanys.any((item) => (item.company_name == work_company));
      if (!isCompany) {
        Provider.of<MyCompanyProvider>(context, listen: false).add(
            myCompanys.length,
            MyCompany(
                company_name: work_company,
                company_address: "",
                company_phone: ""));
      }
      bool isUseCompany = myUseCompanys
          .any((item) => (item.work_use_company == work_use_comapny));
      if (!isUseCompany) {
        Provider.of<MyUseCompanyProvider>(context, listen: false).add(
            myUseCompanys.length,
            MyUseCompany(
                user_uuid:
                    Provider.of<UserInfoProvider>(context, listen: false).uuid,
                work_use_company: work_use_comapny));
      }
      Provider.of<WorkBillProvider>(context, listen: false)
          .updateData(workBill);
      EasyLoading.dismiss();
      EasyLoading.showToast(msg, duration: Duration(milliseconds: 1));

      //更新搜索页面的数据
      GlobalEventBus().event.fire(UpdataSearch());

      Future.delayed(Duration(seconds: 1), () {
        Navigator.pop(context);
      });
    }, (errMsg) {
      EasyLoading.dismiss();
      EasyLoading.showToast(errMsg);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    bus.off("chang_company");
    bus.off("change_usecompany");
  }
}
