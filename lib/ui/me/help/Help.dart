import 'package:flutter/material.dart';

class Help extends StatelessWidget {
  TextStyle textStyle = TextStyle(
    fontSize: 18,
    color: Colors.black,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("使用指导说明"),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.all(20),
        child: ListView(
          children: <Widget>[
            Text(
              "1、首页添加工作单说明",
              style: textStyle,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "1.1 点击下面图片右下角 + 号并进入添加工作页面,然后”添加工作单“ 进行添加,到此一张工作单添加完毕。",
              style: textStyle,
            ),
            SizedBox(
              height: 10,
            ),
            Image.network("https://ossimg.ysmart.xyz/help_add0.jpeg"),
            SizedBox(
              height: 10,
            ),
            Image.network("https://ossimg.ysmart.xyz/help_add1.jpeg"),
            SizedBox(
              height: 10,
            ),
            Text(
              "1.2 点击一个工作单,可进行编辑或删除",
              style: textStyle,
            ),
            SizedBox(
              height: 10,
            ),
            Image.network("https://ossimg.ysmart.xyz/help_commit.jpeg"),
            SizedBox(
              height: 10,
            ),
            Text(
              "2、工作单统计与excel表格下载说明( 先添加了工作单才有统计数据 )",
              style: textStyle,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "2.1 公司数据统计",
              style: textStyle,
            ),
            SizedBox(
              height: 10,
            ),
            Image.network("https://ossimg.ysmart.xyz/help_count.jpeg"),
            SizedBox(
              height: 10,
            ),
            Text(
              "2.2 合作单位查询及数据筛选然后点击右下角 + 进行表格数据生成并下载选择相应数据的生成的excel表格（ 由于小程序的内部机制在下载表格后系统会自动打开,如果生成的表格需要保存建议在表格显示页面右上角进行发送给自己 文件传输助手 或者客户)",
              style: textStyle,
            ),
            SizedBox(
              height: 10,
            ),
            Image.network("https://ossimg.ysmart.xyz/help_download.jpeg"),
            SizedBox(
              height: 10,
            ),
            Image.network("https://ossimg.ysmart.xyz/help_send.jpeg"),
            SizedBox(
              height: 10,
            ),
            Text(
              "3、公司和合作单位添加",
              style: textStyle,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "3.1 我的页面点击 添加公司 或者 添加使用单位 可进行添加。每一个公司或者合作单位可向左滑动出现删除",
              style: textStyle,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "3.2 如果 添加公司 或者 添加使用单位 中有数据了,再进行工作单添加的时候在 公司名称 或者 使用单位 最右边都行进行数据选择",
              style: textStyle,
            ),
            SizedBox(
              height: 10,
            ),
            Image.network("https://ossimg.ysmart.xyz/help_add_finish.jpeg"),
          ],
        ),
      ),
    );
  }
}
