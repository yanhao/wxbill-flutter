import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:yh/bus/UpdateUseCompany.dart';
import 'package:yh/component/HorizontalLine.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/utils/EventBusManager.dart';
import 'package:yh/utils/Global.dart';
import 'package:yh/utils/GlobalEventBus.dart';

class AddUseCompany extends StatefulWidget {
  @override
  _AddUseCompanyState createState() => _AddUseCompanyState();
}

class _AddUseCompanyState extends State<AddUseCompany> {
  String usecompanyName = "";
  TextEditingController _useNameController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _useNameController = TextEditingController();
    _useNameController.addListener(() {
      setState(() {
        usecompanyName = _useNameController.text;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    if (_useNameController != null) {
      _useNameController = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("添加租借单位"),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              height: 50,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.red,
                          size: 14,
                        ),
                        Text(
                          "租借单位",
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: TextField(
                          controller: _useNameController,
                          decoration: InputDecoration(
                              border: InputBorder.none, hintText: "请输入租借单位"),
                        )),
                      ],
                    ),
                  ),
                  HorizontalLine(
                    height: 0.8,
                    color: Color(int.parse("0xfff2f3f5")),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 30),
              height: 40,
              width: MediaQuery.of(context).size.width / 2.5,
              child: FlatButton(
                onPressed: () {
                  submit();
                },
                child: Text(
                  "添加租借单位",
                  style: TextStyle(fontSize: 15),
                ),
                textColor: Colors.white,
                color: Orange,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<Void> submit() async {
    String name = '${usecompanyName}';

    if (name == '' || name.length <= 0) {
      EasyLoading.showToast("请填写租借单位");
      return null;
    }

    EasyLoading.show(status: "正在添加租借单位中");
    await api_AddUseCompany(
      Provider.of<UserInfoProvider>(context, listen: false).uuid,
      name,
      (msg) {
        EasyLoading.dismiss();
        EasyLoading.showToast("添加成功");
        //需要去重新请求数据
        GlobalEventBus().event.fire(UpdateUseCompany());
      },
      (err) {
        EasyLoading.dismiss();
        EasyLoading.showToast(err);
      },
    );

    setState(() {
      _useNameController.clear();
      usecompanyName = '';
    });
  }
}
