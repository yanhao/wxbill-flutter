import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:yh/bean/MyCompany.dart';
import 'package:yh/bean/MyUseCompany.dart';
import 'package:yh/bus/UpdateCompany.dart';
import 'package:yh/bus/UpdateUseCompany.dart';
import 'package:yh/component/HorizontalLine.dart';
import 'package:yh/providers/MyCompanyProvider.dart';
import 'package:yh/providers/MyUseCompanyProvider.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/ui/me/addcompany/AddCompany.dart';
import 'package:yh/ui/me/addusecompany/AddUseCompany.dart';
import 'package:yh/utils/EventBusManager.dart';
import 'package:yh/utils/GlobalEventBus.dart';

class UseCompanyInfo extends StatefulWidget {
  @override
  _UseCompanyInfoState createState() => _UseCompanyInfoState();
}

class _UseCompanyInfoState extends State<UseCompanyInfo> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GlobalEventBus().event.on<UpdateUseCompany>().listen((event) {
      print("开始刷新zujie数据");
      getMyUseCompany();
    });
  }

  Future<Void> getMyUseCompany() async {
    Provider.of<MyUseCompanyProvider>(context, listen: false).datas.clear();
    String user_uuid =
        Provider.of<UserInfoProvider>(context, listen: false).uuid;
    await api_MyUseCompany(user_uuid, (datas) {
      if (datas.length > 0) {
//        for (var i = 0; i < datas.length; i++) {
//          print("租借公司 ---成功的数据是:" + datas[i].work_use_company);
//        }
        Provider.of<MyUseCompanyProvider>(context, listen: false)
            .addAll(0, datas);
      }
    }, (msg) {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("租借单位信息"),
        centerTitle: true,
      ),
      body: Consumer<MyUseCompanyProvider>(
        builder:
            (BuildContext context, MyUseCompanyProvider values, Widget child) {
          return ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return Slidable(
                child: _item(values.datas[index]),
                actionPane: SlidableScrollActionPane(),
                secondaryActions: <Widget>[
                  Container(
                    margin: EdgeInsets.all(8),
                    child: IconSlideAction(
                      caption: '删除',
                      color: Colors.red,
                      icon: Icons.delete_forever,
                      onTap: () {
                        del(context, values.datas[index]);
                      },
                    ),
                  )
                ],
              );
            },
            itemCount: values.datas.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return AddUseCompany();
          }));
        },
//        tooltip: 'Increment',
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: 30,
        ),
      ),
    );
  }

  Future del(BuildContext context, MyUseCompany myCompany) async {
    EasyLoading.show(status: "正在删除${myCompany.work_use_company}租借单位信息");
    await api_DelUseCompany(
        Provider.of<UserInfoProvider>(context, listen: false).uuid,
        myCompany.uuid, (success) {
      EasyLoading.dismiss();
      Provider.of<MyUseCompanyProvider>(context, listen: false)
          .delCompany("${myCompany.work_use_company}");
      EasyLoading.show(status: "删除成功");
    }, (msg) {
      EasyLoading.dismiss();
      EasyLoading.show(status: "${msg}");
    });
  }

  Widget _item(MyUseCompany data) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(left: 6, right: 6, top: 4, bottom: 4),
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(6),
              alignment: Alignment.centerLeft,
              child: Text(
                "租借单位:${data.work_use_company}",
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
            HorizontalLine(
              height: 1,
              color: Color(int.parse("0xfff2f3f5")),
            ),
          ],
        ),
      ),
    );
  }
}
