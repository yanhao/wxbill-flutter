import 'dart:ffi';

import 'package:city_pickers/city_pickers.dart';
import 'package:city_pickers/modal/result.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:yh/bus/UpdateCompany.dart';
import 'package:yh/component/HorizontalLine.dart';
import 'package:yh/component/ItemText.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/utils/Global.dart';
import 'package:yh/utils/GlobalEventBus.dart';

class AddCompany extends StatefulWidget {
  @override
  _AddCompanyState createState() => _AddCompanyState();
}

class _AddCompanyState extends State<AddCompany> {
  String companyName = "";
  String companyPhone = "";
  String companyCity = "";
  String companyAddress = "";
  TextEditingController _nameController;
  TextEditingController _phoneController;
  TextEditingController _addressController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController();
    _phoneController = TextEditingController();
    _addressController = TextEditingController();

    _nameController.addListener(() {
      setState(() {
        companyName = _nameController.text;
      });
    });

    _phoneController.addListener(() {
      companyPhone = _phoneController.text;
    });

    _addressController.addListener(() {
      companyAddress = _addressController.text;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (_nameController != null) {
      _nameController = null;
    }
    if (_phoneController != null) {
      _phoneController = null;
    }
    if (_addressController != null) {
      _addressController = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("添加公司"),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              height: 50,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.red,
                          size: 14,
                        ),
                        Text(
                          "公司名",
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: TextField(
                          controller: _nameController,
                          decoration: InputDecoration(
                              border: InputBorder.none, hintText: "请输入公司名"),
                        )),
                      ],
                    ),
                  ),
                  HorizontalLine(
                    height: 0.8,
                    color: Color(int.parse("0xfff2f3f5")),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              height: 50,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: 14,
                          height: 14,
                        ),
                        Text(
                          "公司电话",
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: TextField(
                          controller: _phoneController,
                          decoration: InputDecoration(
                              border: InputBorder.none, hintText: "请输入公司电话"),
                        )),
                      ],
                    ),
                  ),
                  HorizontalLine(
                    height: 0.8,
                    color: Color(int.parse("0xfff2f3f5")),
                  ),
                ],
              ),
            ),
            ItemText(
              isShow: false,
              title: "所在地区",
              content:
                  '${companyCity == null || companyCity.length <= 0 ? '请选择地区' : companyCity}',
              onTap: () async {
                Result tempResult = await CityPickers.showCityPicker(
                  context: context,
                  theme: Theme.of(context)
                      .copyWith(primaryColor: Color(0xfffe1314)),
                  // 初始化地址信息
                  cancelWidget: Text(
                    '取消',
                    style: TextStyle(fontSize: 18, color: Color(0xff999999)),
                  ),
                  confirmWidget: Text(
                    '确定',
                    style: TextStyle(fontSize: 18, color: Color(0xfffe1314)),
                  ),
                  height: MediaQuery.of(context).size.height / 3,
                );
                if (tempResult != null) {
                  setState(() {
                    companyCity = tempResult.provinceName +
                        '-' +
                        tempResult.cityName +
                        '-' +
                        tempResult.areaName;
                  });
                }
              },
            ),
            companyCity != '' && companyCity.length > 0
                ? Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    height: 50,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: 14,
                                height: 14,
                              ),
                              Text(
                                "详细地址",
                                style: TextStyle(
                                    fontSize: 16, color: Colors.black),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                  child: TextField(
                                controller: _addressController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请输入详细地址"),
                              )),
                            ],
                          ),
                        ),
                        HorizontalLine(
                          height: 0.8,
                          color: Color(int.parse("0xfff2f3f5")),
                        ),
                      ],
                    ),
                  )
                : Container(),
            Container(
              margin: EdgeInsets.only(top: 30),
              height: 40,
              width: MediaQuery.of(context).size.width / 2.5,
              child: FlatButton(
                onPressed: () {
                  submit();
                },
                child: Text(
                  "添加公司",
                  style: TextStyle(fontSize: 15),
                ),
                textColor: Colors.white,
                color: Orange,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<Void> submit() async {
    String name = '${companyName}';
    String phone = '${companyPhone}';
    String city = '${companyCity}';
    String address = '${companyAddress}';

    if (name == '' || name.length <= 0) {
      EasyLoading.showToast("请填写公司名");
      return null;
    }

    String subAddress = "";
    if (city != '' && city.length > 0) {
      subAddress = city;
    } else if (address != '' && address.length > 0) {
      subAddress += address;
    }
    EasyLoading.show(status: "正在添加公司中");
    await api_AddCompany(
      Provider.of<UserInfoProvider>(context, listen: false).uuid,
      name,
      phone,
      subAddress,
      (msg) {
        EasyLoading.dismiss();
        EasyLoading.showToast("添加成功");
        //需要去重新请求数据
        GlobalEventBus().event.fire(UpdateCompany());
      },
      (err) {
        EasyLoading.dismiss();
        EasyLoading.showToast(err);
      },
    );

    setState(() {
      companyName = '';
      companyPhone = '';
      companyCity = '';
      companyAddress = '';
    });
    _nameController.clear();
    _phoneController.clear();
    _addressController.clear();
  }
}
