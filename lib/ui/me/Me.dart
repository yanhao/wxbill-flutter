import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:yh/bean/MyWorkBillCount.dart';
import 'package:yh/bean/ResponseMsg.dart';
import 'package:yh/component/HorizontalLine.dart';
import 'package:yh/component/ItemClickComponent.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/ui/me/help/Help.dart';
import 'package:yh/ui/me/usecompany/UseCompanyInfo.dart';
import 'package:yh/utils/Global.dart';

import 'companyinfo/CompanyInfo.dart';

class MePage extends StatefulWidget {
  @override
  _MePageState createState() => _MePageState();
}

class _MePageState extends State<MePage> {
  MyWorkBillCount myWorkBillCount;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMyCountWorkBill();
  }

  void addCompany() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => CompanyInfo()));
  }

  void addUseCompany() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return UseCompanyInfo();
    }));
  }

  void onHelp() {
    print("点击了---使用指导");
    Navigator.push(context, MaterialPageRoute(builder: (context) => Help()));
  }

  Future getMyCountWorkBill() async {
    String uuid = Provider.of<UserInfoProvider>(context, listen: false).uuid;
    await api_MyCountWorkBill(uuid, (MyWorkBillCount data) {
      setState(() {
        myWorkBillCount = data;
      });
    }, (msg) {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("我的"),
          centerTitle: true,
        ),
        body: Container(
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              _topUserInfo(),
              HorizontalLine(
                height: 0.8,
                color: Color(
                  int.parse("0xfff2f3f5"),
                ),
              ),
              _billInfo(),
              HorizontalLine(
                height: 10,
                color: Color(int.parse("0xfff2f3f5")),
              ),
              ItemClickComponent(
                lefticons: Icons.add_circle_outline,
                righticons: Icons.arrow_forward_ios,
                title: "添加公司",
                height: 50,
                onItem: () {
                  addCompany();
                },
              ),
              HorizontalLine(
                height: 0.8,
                color: Color(int.parse("0xfff2f3f5")),
                left: 20.0,
              ),
              ItemClickComponent(
                lefticons: Icons.add_circle_outline,
                righticons: Icons.arrow_forward_ios,
                title: "添加租借单位",
                height: 50,
                onItem: () {
                  addUseCompany();
                },
              ),
              HorizontalLine(
                height: 10,
                color: Color(int.parse("0xfff2f3f5")),
              ),
              ItemClickComponent(
                lefticons: Icons.help_outline,
                righticons: Icons.arrow_forward_ios,
                title: "使用指导",
                height: 50,
                onItem: () {
                  onHelp();
                },
              ),
              HorizontalLine(
                height: 0.8,
                color: Color(int.parse("0xfff2f3f5")),
                left: 20.0,
              ),
            ],
          ),
        ));
  }

  Widget _topUserInfo() {
    return Padding(
      padding: EdgeInsets.only(top: 36, left: 20, right: 20, bottom: 20),
      child: Row(
        children: <Widget>[
          Image.asset(
            "assets/images/icon_app.png",
            width: 50,
            height: 50,
          ),
          SizedBox(
            width: 20,
          ),
          Text(
            "登录账号: ${Provider.of<UserInfoProvider>(context, listen: false).phone}",
            style: TextStyle(
                color: Orange, fontSize: 18, fontWeight: FontWeight.w300),
          ),
        ],
      ),
    );
  }

  Widget _billInfo() {
    return Column(
      children: <Widget>[
        Container(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[Text("总工单"), Text("总金额(元)")],
          ),
        ),
        HorizontalLine(
          height: 0.8,
          color: Color(int.parse("0xfff2f3f5")),
        ),
        Container(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(myWorkBillCount == null
                  ? "0"
                  : "${myWorkBillCount.billworks}"),
              Text(myWorkBillCount == null ? "0" : "${myWorkBillCount.money}")
            ],
          ),
        ),
      ],
    );
  }
}
