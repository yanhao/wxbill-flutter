import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:yh/bean/MyCompany.dart';
import 'package:yh/bus/UpdateCompany.dart';
import 'package:yh/providers/MyCompanyProvider.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/ui/me/addcompany/AddCompany.dart';
import 'package:yh/utils/EventBusManager.dart';
import 'package:yh/utils/GlobalEventBus.dart';

class CompanyInfo extends StatefulWidget {
  @override
  _CompanyInfoState createState() => _CompanyInfoState();
}

class _CompanyInfoState extends State<CompanyInfo> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GlobalEventBus().event.on<UpdateCompany>().listen((event) {
      print("开始刷新公司信息数据");
      getMyCompany();
    });
  }

  Future<Void> getMyCompany() async {
    Provider.of<MyCompanyProvider>(context, listen: false).datas.clear();
    String user_uuid =
        Provider.of<UserInfoProvider>(context, listen: false).uuid;
    await api_MyCompany(user_uuid, (datas) {
      if (datas.length > 0) {
        for (var i = 0; i < datas.length; i++) {
          print("公司 ---成功的数据是:" +
              datas[i].company_name +
              " uuid=${datas[i].uuid}");
        }
        Provider.of<MyCompanyProvider>(context, listen: false).addAll(0, datas);
      }
    }, (msg) {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("公司信息"),
        centerTitle: true,
      ),
      body: Consumer<MyCompanyProvider>(
        builder:
            (BuildContext context, MyCompanyProvider values, Widget child) {
          return ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return Slidable(
                child: _item(values.datas[index]),
                actionPane: SlidableScrollActionPane(),
                secondaryActions: <Widget>[
                  Container(
                    margin: EdgeInsets.all(8),
                    child: IconSlideAction(
                      caption: '删除',
                      color: Colors.red,
                      icon: Icons.delete_forever,
                      onTap: () {
                        del(context, values.datas[index]);
                      },
                    ),
                  )
                ],
              );
            },
            itemCount: values.datas.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return AddCompany();
          }));
        },
//        tooltip: 'Increment',
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: 30,
        ),
      ),
    );
  }

  Future del(BuildContext context, MyCompany myCompany) async {
    EasyLoading.show(status: "正在删除${myCompany.company_name}公司信息");
    await api_DelCompany(
        Provider.of<UserInfoProvider>(context, listen: false).uuid,
        myCompany.uuid, (success) {
      EasyLoading.dismiss();
      Provider.of<MyCompanyProvider>(context, listen: false)
          .delCompany("${myCompany.company_name}");
      EasyLoading.show(status: "删除成功");
    }, (msg) {
      EasyLoading.dismiss();
      EasyLoading.show(status: "${msg}");
    });
  }

  Widget _item(MyCompany data) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(left: 6, right: 6, top: 4, bottom: 4),
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(6),
              alignment: Alignment.centerLeft,
              child: Text(
                "公司名:${data.company_name}",
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
            Container(
              padding: EdgeInsets.all(6),
              alignment: Alignment.centerLeft,
              child: Text(
                "电话:${data.company_phone}",
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
            Container(
              padding: EdgeInsets.all(6),
              alignment: Alignment.centerLeft,
              child: Text(
                "地址:${data.company_address}",
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
