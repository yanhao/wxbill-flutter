import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:gzx_dropdown_menu/gzx_dropdown_menu.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:yh/bean/WorkBill.dart';
import 'package:yh/component/HorizontalLine.dart';
import 'package:yh/component/VerticalLine.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/providers/WorkBillProvider.dart';
import 'package:yh/service/ApiService.dart';
import 'package:yh/service/Config.dart';
import 'package:yh/ui/home/homeserch/HomeSearch.dart';
import 'package:yh/utils/Global.dart';

class CountBillInfo extends StatefulWidget {
  String companyName;

  CountBillInfo(this.companyName);

  @override
  _CountBillInfoState createState() => _CountBillInfoState();
}

class _CountBillInfoState extends State<CountBillInfo> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey _stackKey = GlobalKey();
  TextEditingController _textController;
  bool isClear = false;
  String searchKey = "";
  int pageindex = 1;
  int pagenumber = 10;
  int year, month;

  List<WorkBill> list = [];
  List<WorkBill> _tempDatas = [];

  List<String> _dropDownHeaderItemStrings; // = ['全城', '品牌'];
  List<SortCondition> _years = [];
  List<SortCondition> _month = [];
  int selectYear, selectMonth;

  GZXDropdownMenuController _dropdownMenuController =
      GZXDropdownMenuController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var now = new DateTime.now();
    setState(() {
      list = Provider.of<WorkBillProvider>(context, listen: false).datas;
      year = now.year;
      month = now.month;
      _dropDownHeaderItemStrings = ['${year}年', '全部'];
      selectYear = year;
      selectMonth = 0;
    });

    _textController = TextEditingController();

    for (int i = 2017; i <= year; i++) {
      if (i == year) {
        _years.add(SortCondition(name: '${year}年', isSelected: true));
      } else {
        _years.add(SortCondition(name: '${i}年', isSelected: false));
      }
    }

    for (int i = 0; i < 13; i++) {
      if (i == 0) {
        _month.add(SortCondition(name: '全部', isSelected: true));
      } else {
        _month.add(SortCondition(name: '${i}月', isSelected: false));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.companyName}"),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.white,
        child: Stack(
          key: _stackKey,
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: _search(),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: 0, right: 20, top: 10, bottom: 10),
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () {
                          searchDatas();
                        },
                        child: Text(
                          "搜索",
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                    ),
                  ],
                ),
                // 下拉菜单头部
                GZXDropDownHeader(
                  // 下拉的头部项，目前每一项，只能自定义显示的文字、图标、图标大小修改
                  items: [
                    GZXDropDownHeaderItem(_dropDownHeaderItemStrings[0]),
                    GZXDropDownHeaderItem(_dropDownHeaderItemStrings[1])
                  ],
                  // GZXDropDownHeader对应第一父级Stack的key
                  stackKey: _stackKey,
                  // controller用于控制menu的显示或隐藏
                  controller: _dropdownMenuController,
                  // 当点击头部项的事件，在这里可以进行页面跳转或openEndDrawer
                  onItemTap: (index) {
                    if (index == 3) {
                      _scaffoldKey.currentState.openEndDrawer();
                    }
                  },
//                // 头部的高度
                  height: 50,
//                // 头部背景颜色
                  color: Colors.white,
//                // 头部边框宽度
//                borderWidth: 1,
//                // 头部边框颜色
//                borderColor: Color(0xFFeeede6),
//                // 分割线高度
//                dividerHeight: 20,
//                // 分割线颜色
//                dividerColor: Color(0xFFeeede6),
//                // 文字样式
                  style: TextStyle(color: Colors.black, fontSize: 18),
//                // 下拉时文字样式
                  dropDownStyle: TextStyle(
                    fontSize: 18,
                    color: Theme.of(context).primaryColor,
                  ),
//                // 图标大小
                  iconSize: 22,
//                // 图标颜色
                  iconColor: Colors.black,
//                // 下拉时图标颜色
                  iconDropDownColor: Theme.of(context).primaryColor,
                ),
                Expanded(
                  child: ListView.builder(
                    itemBuilder: (BuildContext context, int index) {
                      return _itemView(_tempDatas[index]);
                    },
                    itemCount: (_tempDatas != null && _tempDatas.length > 0)
                        ? _tempDatas.length
                        : 0,
//                    itemCount: listData.length,
//          physics: new AlwaysScrollableScrollPhysics(), //保持滚动
                  ),
                ),
              ],
            ),
            // 下拉菜单
            GZXDropDownMenu(
              // controller用于控制menu的显示或隐藏
              controller: _dropdownMenuController,
              // 下拉菜单显示或隐藏动画时长
              animationMilliseconds: 500,
              // 下拉菜单，高度自定义，你想显示什么就显示什么，完全由你决定，你只需要在选择后调用_dropdownMenuController.hide();即可
              menus: [
                GZXDropdownMenuBuilder(
                    dropDownHeight: 40 * 8.0,
                    dropDownWidget: _buildListWidget(_years, (selectValue) {
//                      _dropDownHeaderItemStrings[0] = selectValue;
                      _dropDownHeaderItemStrings[0] = selectValue.name;
                      _dropdownMenuController.hide();
                      setState(() {
                        selectYear = int.parse(selectValue.name
                            .substring(0, selectValue.name.length - 1));
                      });
                      searchDatas();
                    })),
                GZXDropdownMenuBuilder(
                    dropDownHeight: 40 * 8.0,
                    dropDownWidget: _buildListWidget(_month, (selectValue) {
                      _dropDownHeaderItemStrings[1] = selectValue.name;
                      _dropdownMenuController.hide();
                      String value = selectValue.name;
                      setState(() {
                        if (value == '全部') {
                          selectMonth = 0;
                        } else {
                          selectMonth = int.parse(selectValue.name
                              .substring(0, selectValue.name.length - 1));
                        }
                      });
                      searchDatas();
                    })),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          List<int> list = [];
          for (var item in _tempDatas) {
            if (item.check) {
              list.add(item.id);
            }
          }
          //Stringlist json
          if (list == null || list.length <= 0) {
            EasyLoading.showToast("请选择需要下载的工单");
            return;
          }
          await api_CreateExcel(
            Provider.of<UserInfoProvider>(context, listen: false).uuid,
            Provider.of<UserInfoProvider>(context, listen: false).phone,
            "${list}",
            selectYear,
            selectMonth,
            widget.companyName,
            searchKey,
            pageindex,
            pagenumber,
            (data) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text("下载"),
                      content: Text("文件表格已在云单准备完成是否进行下载"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text(
                            "取消",
                            style: TextStyle(color: Colors.black),
                          ),
                          onPressed: () => Navigator.of(context).pop(), //关闭对话框
                        ),
                        FlatButton(
                          child: Text("下载"),
                          onPressed: () {
                            Navigator.of(context).pop(true); //关闭对话框
                            //从服务器下载文件
//                            String url = "https://oss.ysmart.xyz/" + path;
                            _launchURL("${OSS}${data}");
                          },
                        ),
                      ],
                    );
                  });
            },
            (msg) {},
          );
        },
        child: const Icon(Icons.file_download),
      ),
    );
  }

  _launchURL(String path) async {
    if (await canLaunch(path)) {
      await launch(path);
    } else {
      EasyLoading.showToast("系统错误,打开浏览器失败");
    }
  }

  void searchDatas() async {
    setState(() {
      _tempDatas = [];
    });

    EasyLoading.show(status: "正在加载数据中...");
    await api_CountCompanyInfo(
        Provider.of<UserInfoProvider>(context, listen: false).uuid,
        widget.companyName,
        searchKey,
        pageindex,
        pagenumber,
        "${selectYear}",
        "${selectMonth}", (items) {
      EasyLoading.dismiss();
      setState(() {
        _tempDatas = items;
      });
    }, (msg) {
      EasyLoading.dismiss();
      EasyLoading.showToast(msg);
    });
//    _tempDatas = list;
//    List<WorkBill> temps = [];
//    if (selectMonth > 0) {
//      EasyLoading.show(status: "正在搜索");
//      if (searchKey == '') {
//        for (var item in _tempDatas) {
//          int itemYear = int.parse(item.work_time.split("-")[0]);
//          int itemMonth = int.parse(item.work_time.split("-")[1]);
//          if (itemYear == selectYear &&
//              itemMonth == selectMonth &&
//              item.company_name == "${widget.companyName}") {
//            temps.add(item);
//          }
//        }
//      } else {
//        for (var item in _tempDatas) {
//          int itemYear = int.parse(item.work_time.split("-")[0]);
//          int itemMonth = int.parse(item.work_time.split("-")[1]);
//          if (item.work_unit.contains(searchKey) &&
//              itemYear == selectYear &&
//              itemMonth == selectMonth &&
//              item.work_unit == "${widget.companyName}") {
//            temps.add(item);
//          }
//        }
//      }
//      EasyLoading.dismiss();
//      if (temps.length > 0) {
//        setState(() {
//          _tempDatas = temps;
//        });
//      } else {
//        setState(() {
//          _tempDatas = [];
//        });
//        EasyLoading.showToast("暂无数据");
//      }
//    } else if (selectMonth == 0) {
//      EasyLoading.show(status: "正在搜索");
//      if (searchKey == '') {
//        for (var item in _tempDatas) {
//          int itemYear = int.parse(item.work_time.split("-")[0]);
//          if (itemYear == selectYear &&
//              item.company_name == "${widget.companyName}") {
//            temps.add(item);
//          }
//        }
//      } else {
//        for (var item in _tempDatas) {
//          int itemYear = int.parse(item.work_time.split("-")[0]);
//          if (item.work_unit.contains(searchKey) &&
//              itemYear == selectYear &&
//              item.company_name == "${widget.companyName}") {
//            temps.add(item);
//          }
//        }
//      }
//
//      EasyLoading.dismiss();
//      if (temps.length > 0) {
//        setState(() {
//          _tempDatas = temps;
//        });
//      } else {
//        setState(() {
//          _tempDatas = [];
//        });
//        EasyLoading.showToast("暂无数据");
//      }
//    }
  }

  int sex = 1;

  Widget _itemView(WorkBill workBill) {
//  Widget _itemView() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ClipOval(
          child: Checkbox(
            value: workBill.check,
            activeColor: Orange,
            onChanged: (bool val) {
              // val 是布尔值
              this.setState(() {
                workBill.check = val;
              });
            },
          ),
        ),
        Expanded(
          child: Container(
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    setState(() {
                      workBill.isVisibilty = !workBill.isVisibilty;
                    });
                  },
                  child: Container(
                    height: 50,
                    margin: EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "租借单位：",
                              style:
                                  TextStyle(fontSize: 18, color: Colors.black),
                            ),
                            Text("${workBill.work_unit}",
                                style: TextStyle(
                                    fontSize: 18, color: Colors.black)),
                          ],
                        )),
                        workBill.isVisibilty
                            ? Icon(
                                Icons.keyboard_arrow_up,
                                size: 36,
                                color: Orange,
                              )
                            : Icon(
                                Icons.keyboard_arrow_down,
                                size: 36,
                                color: Orange,
                              ),
                      ],
                    ),
                  ),
                ),
                HorizontalLine(
                  height: 0.8,
                  left: 20,
                  color: Color(
                    int.parse("0xfff2f3f5"),
                  ),
                ),
                Visibility(
                    visible: workBill.isVisibilty,
                    child: Column(
                      children: <Widget>[
                        _itemTop("用工时间", "${workBill.work_time}",
                            Colors.black87, true),
                        _itemTop("用工设备", "${workBill.work_device}",
                            Colors.black87, true),
                        _itemTop("工作内容", "${workBill.work_content}",
                            Colors.black87, false),
                        HorizontalLine(
                          height: 0.8,
                          color: Color(
                            int.parse("0xfff2f3f5"),
                          ),
                        ),
                        _itemBottom("设备车牌", "使用时长", "金额(元)"),
                        HorizontalLine(
                          height: 0.8,
                          color: Color(
                            int.parse("0xfff2f3f5"),
                          ),
                        ),
                        _itemBottom(
                            "${workBill.work_car_number}",
                            "${workBill.work_use_time}",
                            "${workBill.work_money}"),
                        HorizontalLine(
                          height: 0.8,
                          color: Color(
                            int.parse("0xfff2f3f5"),
                          ),
                        ),
                      ],
                    ))
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _itemTop(String titleName, String value, Color colors, bool showLine) {
    return Container(
      height: 50,
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: <Widget>[
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "${titleName}",
                style: TextStyle(fontSize: 16, color: Colors.black),
              ),
              Text("${value}", style: TextStyle(fontSize: 15, color: colors)),
            ],
          )),
          showLine
              ? HorizontalLine(
                  height: 0.8,
                  color: Color(
                    int.parse("0xfff2f3f5"),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  Widget _itemBottom(String box1, String box2, String box3) {
    return Container(
      height: 40,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                "${box1}",
                style: TextStyle(fontSize: 15, color: Colors.black87),
              ),
            ),
          ),
          VerticalLine(
            height: 35,
            width: 1,
            color: Color(
              int.parse("0xfff2f3f5"),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text("${box2}",
                  style: TextStyle(fontSize: 15, color: Colors.black87)),
            ),
          ),
          VerticalLine(
            height: 35,
            width: 1,
            color: Color(
              int.parse("0xfff2f3f5"),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text("${box3}",
                  style: TextStyle(fontSize: 15, color: Colors.black87)),
            ),
          ),
        ],
      ),
    );
  }

  Widget _search() {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
        //设置 child 居中
        alignment: Alignment(0, 0),
        height: 40,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: Color(int.parse("0xffF6F9F9")),
          //设置四周边框
          border: new Border.all(width: 1, color: Colors.transparent),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
                child: TextField(
              controller: _textController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding:
                      EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 10),
                  hintText: "请输入租借单位关键词搜索"),
              onChanged: (v) {
                if (v.length > 0) {
                  searchKey = v;
                  setState(() {
                    isClear = true;
                  });
                } else {
                  setState(() {
                    isClear = false;
                  });
                }
              },
            )),
            Padding(
              padding: EdgeInsets.only(left: 5, right: 5),
              child: isClear
                  ? InkWell(
                      onTap: () {
                        if (_textController != null) {
                          _textController.clear();
                          setState(() {
                            searchKey = "";
                            isClear = false;
                            _tempDatas = [];
                          });
                        }
                      },
                      child: Icon(
                        Icons.cancel,
                        color: Colors.black38,
                      ),
                    )
                  : Container(),
            ),
          ],
        ),
      ),
    );
  }

  _buildListWidget(items, void itemOnTap(SortCondition sortCondition)) {
    return ListView.separated(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemCount: items.length,
      // item 的个数
      separatorBuilder: (BuildContext context, int index) =>
          Divider(height: 1.0),
      // 添加分割线
      itemBuilder: (BuildContext context, int index) {
        SortCondition goodsSortCondition = items[index];
        return GestureDetector(
          onTap: () {
            for (var value in items) {
              value.isSelected = false;
            }
            goodsSortCondition.isSelected = true;

            itemOnTap(goodsSortCondition);
          },
          child: Container(
//            color: Colors.blue,
            height: 40,
            child: Row(
              children: <Widget>[
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: Text(
                    goodsSortCondition.name,
                    style: TextStyle(
                      fontSize: 16,
                      color: goodsSortCondition.isSelected
                          ? Theme.of(context).primaryColor
                          : Colors.black,
                    ),
                  ),
                ),
                goodsSortCondition.isSelected
                    ? Icon(
                        Icons.check,
                        color: Theme.of(context).primaryColor,
                        size: 16,
                      )
                    : SizedBox(),
                SizedBox(
                  width: 16,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
