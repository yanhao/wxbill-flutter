import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:provider/provider.dart';
import 'package:yh/bean/Count.dart';
import 'package:yh/component/HorizontalLine.dart';
import 'package:yh/component/VerticalLine.dart';
import 'package:yh/providers/UserInfo_Provider.dart';
import 'package:yh/service/ApiService.dart';

import 'countbill/CountBillInfo.dart';

class CountPage extends StatefulWidget {
  @override
  _CountPageState createState() => _CountPageState();
}

class _CountPageState extends State<CountPage> {
  EasyRefreshController _refreshController = EasyRefreshController();
  bool isRefresh, isLoadMore;
  int pageindex = 1, pagenumber = 20;
  String user_uuid;
  List<Count> listData = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUuid();
    getCounts(true, false, pageindex, pagenumber);
  }

  void getUuid() {
    String uuid = Provider.of<UserInfoProvider>(context, listen: false).uuid;
    setState(() {
      user_uuid = uuid;
    });
  }

  void getCounts(
      bool isRefresh, bool isLoadMore, int pageindex, int pagenumber) async {
    await api_Counts(isRefresh, isLoadMore, pageindex, pagenumber, user_uuid,
        (counts) {
      if (counts.length > 0) {
        if (isRefresh) {
          if (listData != null) {
            listData.clear();
          }
          setState(() {
            listData = counts;
          });
          _refreshController.finishRefresh(success: true);
        } else {
          int index = listData.length;
          listData.insertAll(index, counts);
          setState(() {
            listData = listData;
          });
          _refreshController.finishLoad(success: true);
        }
        int temp = pageindex + 1;
        setState(() {
          this.pageindex = temp;
        });
      }
    }, (errMsg) {
      if (isRefresh) {
        _refreshController.finishRefresh(success: true);
      } else if (isLoadMore) {
        _refreshController.finishLoad(success: true);
      }
      EasyLoading.showToast(errMsg);
    });
  }

  Widget _item(Count count) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              /*builder: (BuildContext context){
            return CountBillInfo();
          }*/
              builder: (BuildContext context) =>
                  CountBillInfo(count.company_name),
            ));
      },
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 50,
              alignment: Alignment.center,
              child: Text(
                "${count.company_name}",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.w400),
              ),
            ),
            HorizontalLine(
              height: 1,
              color: Color(
                int.parse("0xfff2f3f5"),
              ),
            ),
            Container(
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: Container(
                    alignment: Alignment.center,
                    child: Text("工作单(张)"),
                  )),
                  VerticalLine(
                    height: 50,
                    width: 1,
                    color: Color(
                      int.parse("0xfff2f3f5"),
                    ),
                  ),
                  Expanded(
                      child: Container(
                    alignment: Alignment.center,
                    child: Text("金额(元)"),
                  )),
                ],
              ),
            ),
            HorizontalLine(
              height: 1,
              color: Color(
                int.parse("0xfff2f3f5"),
              ),
            ),
            Container(
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: Container(
                    alignment: Alignment.center,
                    child: Text("${count.count}"),
                  )),
                  VerticalLine(
                    height: 50,
                    width: 1,
                    color: Color(
                      int.parse("0xfff2f3f5"),
                    ),
                  ),
                  Expanded(
                      child: Container(
                    alignment: Alignment.center,
                    child: Text("${count.money}"),
                  )),
                ],
              ),
            ),
            HorizontalLine(
              height: 10,
              color: Color(
                int.parse("0xfff2f3f5"),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("统计"),
        centerTitle: true,
      ),
      body: Container(
          color: Colors.white,
          child: EasyRefresh(
            controller: _refreshController,
            enableControlFinishRefresh: true,
            enableControlFinishLoad: true,
            header: ClassicalHeader(),
            footer: ClassicalFooter(),
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return _item(listData[index]);
              },
              itemCount: listData.length,
//          physics: new AlwaysScrollableScrollPhysics(), //保持滚动
            ),
            onRefresh: () async {
              setState(() {
                pageindex = 1;
              });
              getCounts(true, false, 1, pagenumber);
            },
            onLoad: () async {
              getCounts(false, true, pageindex, pagenumber);
            },
          )),
    );
  }
}
