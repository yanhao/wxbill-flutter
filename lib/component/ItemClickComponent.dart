import 'package:flutter/material.dart';

class ItemClickComponent extends StatelessWidget {
  IconData lefticons;
  IconData righticons;
  String title;
  double height;
  VoidCallback onItem;

  ItemClickComponent(
      {Key key, this.lefticons, this.righticons, this.title, this.height,this.onItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onItem,
      child: Container(
        height: height,
        padding: EdgeInsets.only(left: 20, right: 20),
        child: Row(
          children: <Widget>[
            Icon(
              lefticons,
              color: Colors.black45,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(child: Text(title)),
            Icon(righticons, color: Colors.black45),
          ],
        ),
      ),
    );
  }
}
