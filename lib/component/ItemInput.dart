import 'package:flutter/material.dart';
import 'package:yh/bus/DisPoseTextListener.dart';
import 'package:yh/bus/TextChangeEventBus.dart';
import 'package:yh/bus/TextSetDefaultEventBus.dart';
import 'package:yh/utils/EventBusManager.dart';

import 'HorizontalLine.dart';

typedef InputValueCallBack(String v);
typedef ControllerCallBack(TextEditingController controller);
typedef RightClick();
typedef ClearCallBack();

class ItemInput extends StatefulWidget {
  String widgetname;
  String title;
  String hint;
  String defaultValue;
  bool showStatr;
  bool showArrow;
  TextInputType keyboardType;
  InputValueCallBack onChange;
  RightClick click;

  ItemInput(
      {Key key,
      this.widgetname,
      this.title,
      this.hint,
      this.defaultValue,
      this.showStatr,
      this.showArrow,
      this.keyboardType,
      this.onChange,
      this.click})
      : super(key: key);

  @override
  _ItemInputState createState() => _ItemInputState();
}

class _ItemInputState extends State<ItemInput> {
  TextEditingController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = TextEditingController();
    String d_Value = widget.defaultValue;
    if (d_Value != null || d_Value != '') {
      controller.text = d_Value;
    }
//    EventBusManager().eventBus.on<TextChangeEventBus>().listen((event) {
//      controller.text = event.value;
//      controller.clear();
//    });
//
//    EventBusManager().eventBus.on<TextSetDefaultEventBus>().listen((event) {
//      if ("${event.widgteName}" == "${widget.widgetname}") {
//        controller.text = event.value;
//      }
//    });

//    EventBusManager().eventBus.on<DisPoseTextListener>().listen((event) {
//      if (controller != null) {
//        controller.dispose();
//      }
//    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (controller != null) {
      controller.dispose();
    }
//    EventBusManager().eventBus.destroy();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      height: 50,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                widget.showStatr
                    ? Icon(
                        Icons.star,
                        color: Colors.red,
                        size: 14,
                      )
                    : Icon(
                        Icons.star,
                        color: Colors.transparent,
                        size: 14,
                      ),
                Text(
                  "${widget.title}",
                  style: TextStyle(fontSize: 16, color: Colors.black),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                    child: TextField(
                  keyboardType: widget.keyboardType,
                  controller: controller,
                  decoration: InputDecoration(
                      border: InputBorder.none, hintText: "${widget.hint}"),
                  onChanged: (v) {
                    widget.onChange(v);
                  },
                  onSubmitted: (v) {
                    print("执行了onSubmitted=" + v);
                  },
                )),
                InkWell(
                  onTap: () {
                    widget.click();
                  },
                  child: widget.showArrow
                      ? Icon(Icons.arrow_forward_ios, color: Colors.black45)
                      : Container(),
                )
              ],
            ),
          ),
          HorizontalLine(
            height: 0.8,
            color: Color(int.parse("0xfff2f3f5")),
          ),
        ],
      ),
    );
  }
}
