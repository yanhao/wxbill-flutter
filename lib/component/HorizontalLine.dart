import 'package:flutter/material.dart';

class HorizontalLine extends StatelessWidget {
  double height;
  Color color;
  double left, right, top, bottom;

  HorizontalLine(
      {Key key,
      this.height,
      this.color,
      this.left = 0.0,
      this.right = 0.0,
      this.top = 0.0,
      this.bottom = 0.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
          EdgeInsets.only(left: left, right: right, top: top, bottom: bottom),
      height: height,
      color: color,
    );
  }
}
