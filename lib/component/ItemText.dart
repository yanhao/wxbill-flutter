import 'package:flutter/material.dart';

import 'HorizontalLine.dart';

class ItemText extends StatefulWidget {
  String title, content;
  bool isShow;

  VoidCallback onTap;

  ItemText({Key key, this.isShow, this.title, this.content, this.onTap})
      : super(key: key);

  @override
  _ItemTextState createState() => _ItemTextState();
}

class _ItemTextState extends State<ItemText> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      height: 50,
      child: Column(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () {
                widget.onTap();
              },
              child: Row(
                children: <Widget>[
                  widget.isShow
                      ? Icon(
                          Icons.star,
                          color: Colors.red,
                          size: 14,
                        )
                      : Container(
                          width: 14,
                          height: 14,
                        ),
                  Text(
                    "${widget.title}",
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      child: Text(
                    "${widget.content}",
                    style: TextStyle(fontSize: 16.0),
                  )),
                  Icon(Icons.arrow_forward_ios, color: Colors.black45),
                ],
              ),
            ),
          ),
          HorizontalLine(
            height: 0.8,
            color: Color(int.parse("0xfff2f3f5")),
          ),
        ],
      ),
    );
    ;
  }
}
