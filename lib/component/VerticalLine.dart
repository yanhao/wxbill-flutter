import 'package:flutter/material.dart';

class VerticalLine extends StatelessWidget {
  double height;
  double width;
  Color color;
  double left, right, top, bottom;

  VerticalLine(
      {Key key,
      this.height,
      this.width,
      this.color,
      this.left = 0.0,
      this.right = 0.0,
      this.top = 0.0,
      this.bottom = 0.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
          EdgeInsets.only(left: left, right: right, top: top, bottom: bottom),
      width: width,
      height: height,
      color: color,
    );
  }
}
