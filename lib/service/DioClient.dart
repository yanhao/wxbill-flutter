import 'package:dio/dio.dart';
import 'package:yh/service/Config.dart';

class DioClient {
  //写一个单例
  //在 Dart 里，带下划线开头的变量是私有变量
  static DioClient _instance;

  static DioClient getInstance() {
    if (_instance == null) {
      _instance = DioClient();
    }
    return _instance;
  }

  Dio dio = new Dio();

  DioClient() {
    dio.options.baseUrl = HOST;
    dio.options.connectTimeout = 5000;
    dio.options.receiveTimeout = 3000;

//    dio.interceptors.add(LogInterceptor(responseBody: GlobalConfig.isDebug)); //是否开启请求日志
//    dio.interceptors.add(CookieManager(CookieJar()));//缓存相关类，具体设置见https://github.com/flutterchina/cookie_jar
  }
}
