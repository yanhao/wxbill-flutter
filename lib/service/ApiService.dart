import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yh/CallBack/LoginCallBack.dart';
import 'package:yh/CallBack/ResponseCallback.dart';
import 'package:yh/bean/Count.dart';
import 'package:yh/bean/LoginRespone.dart';
import 'package:yh/bean/MyCompany.dart';
import 'package:yh/bean/MyUseCompany.dart';
import 'package:yh/bean/MyWorkBillCount.dart';
import 'package:yh/bean/ResponseMsg.dart';
import 'package:yh/bean/WorkBill.dart';
import 'package:yh/bean/WorkDevices.dart';
import 'package:yh/service/Config.dart';
import 'package:yh/service/DioClient.dart';

//Future api_login(String phone, String pwd, LoginCallbakc callbakc) async {
//  Response response;
//  try {
//    response = await DioClient.getInstance().dio.post(LOGIN_API,
//        data: {"phone": phone, "password": pwd},
//        options: Options(contentType: Headers.formUrlEncodedContentType));
////    var result = ResponseMsg<dynamic>.fromJson(json.decode(response.toString())).data;
//    ResponseMsg result =
//        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
//    if (result.code == 200) {
//      LoginRespone loginBean = LoginRespone.fromJson(result.data);
//      callbakc.onSuccess(loginBean);
//    } else {
//      callbakc.onFail(result.message);
//    }
//  } catch (e) {
//    print("登录错误" + e);
//    callbakc.onFail(e.toString());
//  }
//}

//登录
Future api_login(
    String phone,
    String pwd,
    BsseOnSuccessDataCallBack<LoginRespone> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.post(LOGIN_API,
        data: {"phone": phone, "password": pwd},
        options: Options(contentType: Headers.formUrlEncodedContentType));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      LoginRespone loginRespone = LoginRespone.fromJson(result.data);
      onSuccess(loginRespone);
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//检测token
Future api_checkToken(String token, BsseOnSuccessDataCallBack<String> onSuccess,
    BsseOnSuccessDataCallBack<String> onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.get(CHECKTOKEN_API,
        options: Options(headers: {
          "Authorization": 'Bearer ' + token,
        }));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      onSuccess(result.message);
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//获取我名下的公司
Future api_MyCompany(
    String uuid,
    BsseOnSuccessDataCallBack<List<MyCompany>> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.get(
      GETALLCOMPANY,
      queryParameters: {"user_uuid": uuid, 'pageindex': 1, 'pagenumber': 500},
    );
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      List listJson = result.data;
      List<MyCompany> myCompanys =
          listJson.map((item) => MyCompany.fromJson(item)).toList();
      if (myCompanys.length > 0) {
        onSuccess(myCompanys);
      } else {
        onFail("暂无数据");
      }
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//获取我名下的租借公司
Future api_MyUseCompany(
    String uuid,
    BsseOnSuccessDataCallBack<List<MyUseCompany>> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.get(
      GETALLUNITCOMPANY,
      queryParameters: {"user_uuid": uuid, 'pageindex': 1, 'pagenumber': 500},
    );
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      List listJson = result.data;
      List<MyUseCompany> myUseCompany =
          listJson.map((item) => MyUseCompany.fromJson(item)).toList();
      if (myUseCompany.length > 0) {
        onSuccess(myUseCompany);
      } else {
        onFail("暂无数据");
      }
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//我的页面统计信息
Future api_MyCountWorkBill(
    String user_uuid,
    BsseOnSuccessDataCallBack<MyWorkBillCount> onSuccessCallback,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.get(
      ME_WORKBILLCOUNT,
      queryParameters: {"user_uuid": user_uuid},
    );
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      MyWorkBillCount respone = await MyWorkBillCount.fromJson(result.data);
      onSuccessCallback(respone);
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//首页主页面
Future api_HomeBillworks(
    bool isRefresh,
    bool isLoadMore,
    int pageindex,
    int pagenumber,
    String user_uuid,
    BsseOnSuccessDataCallBack<List<WorkBill>> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.get(
      GETWORKBILL,
      queryParameters: {
        "user_uuid": user_uuid,
        "pageindex": pageindex,
        "pagenumber": pagenumber
      },
    );
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      List listJson = result.data;
      List<WorkBill> workBills =
          listJson.map((item) => WorkBill.fromJson(item)).toList();
      if (workBills.length > 0) {
        onSuccess(workBills);
      } else {
        onFail("暂无数据");
      }
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//统计主页面
Future api_Counts(
    bool isRefresh,
    bool isLoadMore,
    int pageindex,
    int pagenumber,
    String user_uuid,
    BsseOnSuccessDataCallBack<List<Count>> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.get(
      COUNTBILLS,
      queryParameters: {
        "user_uuid": user_uuid,
        "pageindex": pageindex,
        "pagenumber": pagenumber
      },
    );
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      List listJson = result.data;
      List<Count> counts =
          listJson.map((item) => Count.fromJson(item)).toList();
      if (counts.length > 0) {
        onSuccess(counts);
      } else {
        onFail("暂无数据");
      }
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//获取设备信息
Future api_Devices(BsseOnSuccessDataCallBack<List<WorkDevices>> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.get(DEVICES);
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      List listJson = result.data;
      List<WorkDevices> counts =
          listJson.map((item) => WorkDevices.fromJson(item)).toList();
      if (counts.length > 0) {
        onSuccess(counts);
      } else {
        onFail("暂无数据");
      }
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//添加工单
Future api_AddWorkBill(
    WorkBill workBill,
    BsseOnSuccessDataCallBack<WorkBill> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.post(ADDWORKBILL,
        data: {
          "user_uuid": workBill.user_uuid,
          "work_time": workBill.work_time,
          "work_unit": workBill.work_unit,
          "work_content": workBill.work_content,
          "work_car_number": workBill.work_car_number,
          "work_device": workBill.work_device,
          "work_use_time": workBill.work_use_time,
          "company_name": workBill.company_name,
          "work_time_price": workBill.work_time_price
        },
        options: Options(contentType: Headers.formUrlEncodedContentType));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      onSuccess(workBill);
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//删除工单
Future api_delWorkBill(String uuid, String user_uuid,
    BaseOnSuccessCallBack onSuccess, BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.get(DELWORKBILL,
        queryParameters: {
          "uuid": uuid,
          "user_uuid": user_uuid,
        },
        options: Options(contentType: Headers.formUrlEncodedContentType));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      onSuccess();
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//更新工单
Future api_UpdateWorkBill(
    WorkBill workBill,
    BsseOnSuccessDataCallBack<String> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.post(UPDATAWORKBILL,
        data: {
          "uuid": workBill.uuid,
          "user_uuid": workBill.user_uuid,
          "work_time": workBill.work_time,
          "work_unit": workBill.work_unit,
          "work_content": workBill.work_content,
          "work_car_number": workBill.work_car_number,
          "work_device": workBill.work_device,
          "work_use_time": workBill.work_use_time,
          "company": workBill.company_name,
          "work_time_price": workBill.work_time_price
        },
        options: Options(contentType: Headers.formUrlEncodedContentType));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      onSuccess(result.message);
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//添加公司
Future api_AddCompany(
    String user_uuid,
    String company_name,
    String company_address,
    String company_phone,
    BsseOnSuccessDataCallBack<String> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.post(ADDCOMPANYNAME,
        data: {
          "user_uuid": user_uuid,
          "company_name": company_name,
          "company_address": company_address,
          "company_phone": company_phone
        },
        options: Options(contentType: Headers.formUrlEncodedContentType));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      print("添加公司成功");
      onSuccess(result.message);
    } else {
      print("添加公司失败");
      onFail(result.message);
    }
  } catch (e) {
    print("添加公司错误信息${e.toString()}");
    onFail(e.toString());
  }
}

//添加租借公司
Future api_AddUseCompany(
    String user_uuid,
    String work_use_company,
    BsseOnSuccessDataCallBack<String> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.post(ADDUNITCOMPANYNAME,
        data: {"user_uuid": user_uuid, "work_use_company": work_use_company},
        options: Options(contentType: Headers.formUrlEncodedContentType));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      print("添加租借公司成功");
      onSuccess(result.message);
    } else {
      print("添加租借公司失败");
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
    print("添加租借公司错误信息${e.toString()}");
  }
}

//删除公司
Future api_DelCompany(
    String user_uuid,
    String uuid,
    BsseOnSuccessDataCallBack<String> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.post(DELCOMPANY,
        data: {
          "user_uuid": user_uuid,
          "uuid": uuid,
        },
        options: Options(contentType: Headers.formUrlEncodedContentType));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
//      print("添加公司成功");
      onSuccess("OK");
    } else {
//      print("添加公司失败");
      onFail(result.message);
    }
  } catch (e) {
//    print("添加公司错误信息${e.toString()}");
    onFail(e.toString());
  }
}

//删除租借公司
Future api_DelUseCompany(
    String user_uuid,
    String uuid,
    BsseOnSuccessDataCallBack<String> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.post(DELUSECOMPANY,
        data: {
          "user_uuid": user_uuid,
          "uuid": uuid,
        },
        options: Options(contentType: Headers.formUrlEncodedContentType));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
//      print("添加公司成功");
      onSuccess("OK");
    } else {
//      print("添加公司失败");
      onFail(result.message);
    }
  } catch (e) {
//    print("添加公司错误信息${e.toString()}");
    onFail(e.toString());
  }
}

Future api_CountCompanyInfo(
    String user_uuid,
    String companyname,
    String unitname,
    int pageindex,
    int pagenumber,
    String year,
    String month,
    BsseOnSuccessDataCallBack onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.get(COUNTCOMPANYINFO,
        queryParameters: {
          "user_uuid": user_uuid,
          "companyname": companyname,
          "unitname": unitname,
          "pageindex": pageindex,
          "pagenumber": pagenumber,
          "year": year,
          "month": month
        },
        options: Options(contentType: Headers.formUrlEncodedContentType));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      List listJson = result.data;
      List<WorkBill> workbill =
          listJson.map((item) => WorkBill.fromJson(item)).toList();
      if (workbill.length > 0) {
        onSuccess(workbill);
      } else {
        onFail("暂无数据");
      }
    } else {
      onFail(result.message);
    }
  } catch (e) {
    onFail(e.toString());
  }
}

//服务器创建excel
Future api_CreateExcel(
    String user_uuid,
    String phone,
    String ids,
    int year,
    int month,
    String companyname,
    String unitname,
    int pageindex,
    int pagenumber,
    BsseOnSuccessDataCallBack<String> onSuccess,
    BaseOnFailCallBack onFail) async {
  Response response;
  try {
    response = await DioClient.getInstance().dio.post(CREATEEXCEL,
        data: {
          "user_uuid": user_uuid,
          "phone": phone,
          "ids": ids,
          "year": year,
          "month": month,
          "companyname": companyname,
          "unitname": unitname,
          "pageindex": pageindex,
          "pagenumber": pagenumber,
        },
        options: Options(contentType: Headers.formUrlEncodedContentType));
    ResponseMsg result =
        ResponseMsg<dynamic>.fromJson(json.decode(response.toString()));
    if (result.code == 200) {
      onSuccess("${result.data}");
    } else {
//      print("添加公司失败");
      onFail(result.message);
    }
  } catch (e) {
//    print("添加公司错误信息${e.toString()}");
    onFail(e.toString());
  }
}
