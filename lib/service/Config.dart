const HOST = "http://192.168.0.101:9633";
const OSS = "https://oss.ysmart.xyz/";
const LOGIN_API = '/billapi/user/login';
const CHECKTOKEN_API = '/billapi/user/check';
const ME_WORKBILLCOUNT = "/billapi/billwork/queryallcount";
const COUNTBILLS = "/billapi/billwork/querycount";
const GETWORKBILL = "/billapi/billwork/querys";
const DEVICES = "/billapi/devices/devices?key=devices";
const ADDWORKBILL = "/billapi/billwork/add";
const DELWORKBILL = "/billapi/billwork/del";
const UPDATAWORKBILL = "/billapi/billwork/update";
const GETALLCOMPANY = "/billapi/billcompany/querys";
const GETALLUNITCOMPANY = "/billapi/billuseunit/querys";
const ADDCOMPANYNAME = "/billapi/billcompany/add";
const ADDUNITCOMPANYNAME = "/billapi/billuseunit/add";
const DELCOMPANY = "/billapi/billcompany/del";
const DELUSECOMPANY = "/billapi/billuseunit/del";
const COUNTCOMPANYINFO = "/billapi/billwork/querycountCompanyinfo";
const CREATEEXCEL = "/billapi/fileexcel/create";
