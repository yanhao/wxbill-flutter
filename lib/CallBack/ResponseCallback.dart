import 'package:yh/bean/Count.dart';
import 'package:yh/bean/LoginRespone.dart';
import 'package:yh/bean/MyWorkBillCount.dart';
import 'package:yh/bean/WorkBill.dart';
import 'package:yh/bean/WorkDevices.dart';

typedef BaseOnFailCallBack(String msg);
typedef BaseOnSuccessCallBack();
typedef BsseOnSuccessDataCallBack<T>(T data);

//typedef CheckTokenOnSuccessCallBack(String v);
//typedef CheckTokenOnFailCallBack(String v);
//typedef LoginOnSuccessCallBack(LoginRespone loginRespone);
//typedef MyWorkBillCountSuccessCallBack(MyWorkBillCount data);
//typedef CountsSuccessCallback(List<Count> counts);
//typedef HomeWorkBillsCallBack(List<WorkBill> workBills);
//typedef DeviceInfosBillsCallBack(List<WorkDevices> datas);
//typedef AddWorkBillonSuccessCallBack(WorkBill datas);
//typedef DelWorkBillonSuccessCallBack();
