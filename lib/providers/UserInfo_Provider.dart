import 'package:flutter/material.dart';

class UserInfoProvider with ChangeNotifier {
  String _token;
  String _uuid;
  String _phone;

  String  get token => _token;
  String  get uuid => _uuid;
  String  get phone => _phone;

  void setToken(String _token) {
    this._token = _token;
    notifyListeners();
  }

  void setUUId(String _uuid) {
    this._uuid = _uuid;
    notifyListeners();
  }

  void setPhone(String _phone) {
    this._phone = _phone;
    notifyListeners();
  }
}
