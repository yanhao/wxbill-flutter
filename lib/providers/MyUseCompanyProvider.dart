import 'package:flutter/material.dart';
import 'package:yh/bean/MyCompany.dart';
import 'package:yh/bean/MyUseCompany.dart';

class MyUseCompanyProvider with ChangeNotifier {
  List<MyUseCompany> _datas = [];

  List<MyUseCompany> get datas => _datas;

  void add(int index, MyUseCompany data) {
    _datas.insert(index, data);
    notifyListeners();
  }

  void addAll(int index, List<MyUseCompany> data) {
    _datas.insertAll(index, data);
    notifyListeners();
  }

  void delCompany(String name) {
//    for (var item in _datas) {
//      if (item.work_use_company == name) {
//        _datas.remove(item);
//      }
//      break;
//    }
//    notifyListeners();

    if (_datas.length > 0) {
      _datas.removeWhere((item) => item.work_use_company == name);
      notifyListeners();
    }
  }
}
