import 'package:flutter/material.dart';
import 'package:yh/bean/MyCompany.dart';

class MyCompanyProvider with ChangeNotifier {
  List<MyCompany> _datas = [];

  List<MyCompany> get datas => _datas;

  void add(int index, MyCompany data) {
    _datas.insert(index, data);
    notifyListeners();
  }

  void addAll(int index, List<MyCompany> data) {
    _datas.insertAll(index, data);
    notifyListeners();
  }

  void delCompany(String name) {
//    for (var item in _datas) {
//      if (item.company_name == name) {
//        _datas.remove(item);
//      }
//      break;
//    }
    if (_datas.length > 0) {
      _datas.removeWhere((item) => item.company_name == name);
      notifyListeners();
    }
  }
}
