import 'package:flutter/material.dart';
import 'package:yh/bean/WorkBill.dart';

class WorkBillProvider with ChangeNotifier {
  List<WorkBill> _datas = [];

  List<WorkBill> get datas => _datas;

  void addAll(int index, List<WorkBill> data) {
    _datas.insertAll(index, data);
    notifyListeners();
  }

  void add(int index, WorkBill data) {
    _datas.insert(index, data);
    notifyListeners();
  }

  void updateIndex(int index, WorkBill data) {
    _datas[index] = data;
    notifyListeners();
  }

  void updateData(WorkBill data) {
    for (var item in _datas) {
      if (item.uuid == data.uuid) {
        item.work_time_price = data.work_time;
        item.work_unit = data.work_unit;
        item.work_content = data.work_content;
        item.work_car_number = data.work_car_number;
        item.work_device = data.work_device;
        item.work_use_time = data.work_use_time;
        item.work_time_price = data.work_time_price;
        item.work_money = data.work_money;
        item.company_name = data.company_name;
        item.work_time = data.work_time;
      }
      break;
    }
    notifyListeners();
  }

  void delIndex(int index) {
    _datas.removeAt(index);
    notifyListeners();
  }

  void delUuid(String uuid) {
    for (var item in _datas) {
      if (item.uuid == uuid) {
        _datas.remove(item);
      }
      break;
    }
    notifyListeners();
  }
}
