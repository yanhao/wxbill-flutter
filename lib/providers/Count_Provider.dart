import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CountProvider with ChangeNotifier {
  List _counts = [];

  List get counts => _counts;

  void add(int position, List data) {
    _counts.insertAll(position, data);
    notifyListeners();
  }
}
