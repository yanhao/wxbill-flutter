import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import 'MyCompanyProvider.dart';
import 'MyUseCompanyProvider.dart';
import 'UserInfo_Provider.dart';
import 'WorkBillProvider.dart';
//SingleChildWidget
//SingleChildStatelessWidget
List<SingleChildStatelessWidget> providers = [
  ChangeNotifierProvider<UserInfoProvider>(
    create: (_) => UserInfoProvider(),
  ),
  ChangeNotifierProvider<WorkBillProvider>(
    create: (_) => WorkBillProvider(),
  ),
  ChangeNotifierProvider<MyCompanyProvider>(
    create: (_) => MyCompanyProvider(),
  ),
  ChangeNotifierProvider<MyUseCompanyProvider>(
    create: (_) => MyUseCompanyProvider(),
  )
];
