import 'package:flutter/material.dart';

const MaterialColor Orange = const MaterialColor(
  _orangePrimaryValue,
  <int, Color>{
    50: Color(0xFFFFF3E0),
    100: Color(0xFFFFE0B2),
    200: Color(0xFFFFCC80),
    300: Color(0xFFFFB74D),
    400: Color(0xFFFFA726),
    500: Color(_orangePrimaryValue),
    600: Color(0xFFFF9800),
    700: Color(0xFFF57C00),
    800: Color(0xFFEF6C00),
    900: Color(0xFFE65100),
  },
);
const int _orangePrimaryValue = 0xFFFF87625; //0xFFFF9800;
