import 'package:shared_preferences/shared_preferences.dart';

//Future<String> getSpToken() async {
//  SharedPreferences prefs = await SharedPreferences.getInstance();
//  String sp_token = await prefs.getString("token");
//  return sp_token;
//}
//
//Future<String> getSpUuid() async {
//  SharedPreferences prefs = await SharedPreferences.getInstance();
//  String sp_uuid = await prefs.getString("uuid");
//  return sp_uuid;
//}
//
//Future<String> getSpPhone() async {
//  SharedPreferences prefs = await SharedPreferences.getInstance();
//  String sp_phone = await prefs.getString("phone");
//  return sp_phone;
//}

Future<String> getSpString(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String sp_value = await prefs.getString(key);
  return sp_value;
}

Future<int> getSpint(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int sp_value = await prefs.getInt(key);
  return sp_value;
}

Future clearSpUserInfo() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.remove("token");
  prefs.remove("uuid");
  prefs.remove("phone");
}
